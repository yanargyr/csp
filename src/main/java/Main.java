/*
 * Main.java
 *
 * Created on December 8, 2007, 10:31 PM
 *
 * To change this template, choose Tools | Template Manager
 * and open the template in the editor.
 */

import csp.data.ProblemClass;
import csp.util.BenchmarkRunner;

import java.util.List;


public class Main {

    public Main() {
    }

    public static void main(String[] args) {
        if (args.length == 9) {
            runSingleProblem(args);
        } else if (args.length == 12) {
            runFullTestSet(args);
        } else if (args.length == 5) {
            runPredefinedTests(args);
        } else if (args.length == 0) {
            runDemo();
        } else if (args.length == 81) {
            solveSudoku(args);
        } else {
            printHelp();
        }
    }



    private static void solveSudoku(String[] args) {
        int[] reserved = new int[81];
        for (int i = 0; i < 81; i++) {
            reserved[i] = Integer.parseInt(args[i]);
        }
        BenchmarkRunner.solveSudoku(reserved, 9);
    }

    private static void runPredefinedTests(String[] args) {
        int pclass = Integer.parseInt(args[0]);
        int tests = Integer.parseInt(args[1]);
        int timeLimit = Integer.parseInt(args[2]);
        int seed = Integer.parseInt(args[4]);
        int heuristic = Integer.parseInt(args[3]);

        List<ProblemClass> classes = ProblemClass.getDemoClasses();
        try {
            BenchmarkRunner.runFullTestSet(classes.get(pclass - 1), 1, tests, timeLimit, seed, heuristic);
        } catch (Exception e) {
            System.out.println(pclass + " is not a problem class.");
        }
    }

    private static void runFullTestSet(String[] args) {

        int vars = Integer.parseInt(args[0]);
        int dom = Integer.parseInt(args[1]);
        int min_arity = Integer.parseInt(args[2]);
        int max_arity = Integer.parseInt(args[3]);
        /*
         Constraint probability. 0 will create 0 constraint. 1 will create the max available constraints,
         For a = fixed arity, this would be:
         totalVars! / (a! * (totalVars - a)!)
         */
        double fixedP = Double.parseDouble(args[4]);
        double minq = 1 - Double.parseDouble(args[5]);
        double maxq = 1 - Double.parseDouble(args[6]);
        int runs_q = Integer.parseInt(args[7]);
        int runs_q2 = Integer.parseInt(args[8]);
        int timeLimit = Integer.parseInt(args[9]);
        int seed = Integer.parseInt(args[11]);
        int heuristic = Integer.parseInt(args[10]);

        BenchmarkRunner.runFullTestSet(vars, dom, min_arity, max_arity, fixedP, minq, maxq, runs_q, runs_q2, timeLimit, seed, heuristic);
    }

    private static void runSingleProblem(String[] args) {
        int vars = Integer.parseInt(args[0]);
        int dom = Integer.parseInt(args[1]);
        int min_arity = Integer.parseInt(args[2]);
        int max_arity = Integer.parseInt(args[3]);
        double p = Double.parseDouble(args[4]);
        double q = 1 - Double.parseDouble(args[5]);
        int timeLimit = Integer.parseInt(args[6]);
        int heuristic = Integer.parseInt(args[7]);
        int seed = Integer.parseInt(args[8]);

        BenchmarkRunner.runSingleProblem(vars, dom, min_arity, max_arity, p, q, timeLimit, heuristic, seed);
    }

    private static void runDemo() {
        System.out.println("\n\n\n***************** SUDOKU DEMO *****************");
        BenchmarkRunner.runSudokuVeryHard();

        System.out.println("\n\n\n***************** 8-Queens DEMO *****************");
        BenchmarkRunner.solveNQueenProblem(8);

        System.out.println("\n\n\n ---- use '-help' flag for more info on the --- : \n\n");
    }

    private static void printHelp() {
        System.out.println("\n\n\n ---- CSP solver usage --- : \n\n");
        //System.out.print("\n1)for one single test :\n");
        //System.out.print(" />java -jar '/jar source../CSP12.jar' var dom m M p q t \n");

        System.out.println("\n\n\n Running Single test:");


        System.out.println("\n\n\n 1) Running Single test: (8 args)");
        System.out.println(" $ java -jar '/path to the jar../CSP.jar' totalVars dom min_arity max_arity p q timeLimit heuristic seed  \n");
        System.out.println("0 vars:         Number of variables");
        System.out.println("1 dom:          Domain size");
        System.out.println("2 min_arity:    minimum (initial) arity of the constraints");
        System.out.println("3 max_arity:    maximum (initial) arity of the constraints");
        System.out.println("4 P:            probability to create constraint (double in (0,1]) ");
        System.out.println("5 Q:            constraint tightness (double in [0,1])");
        System.out.println("6 timeLimit:    Time out limit, in seconds");
        System.out.println("7 heuristic:    Branching csp.heuristics.HeuristicVarOrdering to use (see below)");
        System.out.println("8 seed:         Seed for the random problem generator (Int)");

        System.out.println("\n\n\n 2) Running multiple tests predefined problem classes: (12 args)");
        System.out.println(" $ java -jar '/path to the jar../CSP.jar' class tests timeLimit heuristic seed  \n");
        System.out.println("0 class:        Problem Class (see below)");
        System.out.println("1 tests:        number of tests (integer > 1) ");
        System.out.println("2 timeLimit:    Time out limit, in seconds");
        System.out.println("3 heuristic:    Branching Heuristics to use (see below)");
        System.out.println("4 seed:         Seed for the random problem generator (Int)");


        System.out.println("\n\n\n 3) Running multiple tests with varying tightness: (12 args)");
        System.out.println(" $ java -jar '/path to the jar../CSP.jar' var dom min_arity max_arity fixedP minq maxq steps_q tests timeLimit heuristic seed  \n");
        System.out.println("0 vars:         Number of variables");
        System.out.println("1 dom:          Domain size");
        System.out.println("2 min_arity:    minimum (initial) arity of the constraints");
        System.out.println("3 max_arity:    maximum (initial) arity of the constraints");
        System.out.println("4 fixedP:       probability to create constraint (double in (0,1]) ");
        System.out.println("5 minq:         min/starting proportion of allowed tuples (1-q, q = tightness) (double in (0,1))");
        System.out.println("6 maxq:         max constraint proportion of allowed tuples (1-q, q = tightness) (double in [0, 1], maxq < minq)");
        System.out.println("7 steps_q:      variations/steps of q (integer > 1)");
        System.out.println("8 tests:        tests per step_q (integer > 1) ");
        System.out.println("9 timeLimit:    Time out limit, in seconds");
        System.out.println("10 heuristic:   Branching heuristics to use (see below)");
        System.out.println("11 seed:        Seed for the random problem generator (Int)");

        System.out.println("\n\n\n 4) Solve Sudoku");
        System.out.println(" $ java -jar '/path to the jar../CSP.jar' 4 0 0 8 9 .... 81 ints in total. Fill all 81 numbers from a real sudoku problem, with space char between them. Fill in 0 for blank cells \n");

        System.out.println("\n\n\n Problem classes: \n_____________________\n");
        System.out.println("#   <arity, vars, domain, p, 1-q>");

        // Classes from SETN 08 paper
        List<ProblemClass> classes = ProblemClass.getDemoClasses();
        int i = 0;
        for(ProblemClass c : classes) {
            i++;
            System.out.print("" + i + "." );
            c.printClassDescription();
        }

        System.out.println("\n\n\n heuristics parameters: \n_____________________\n");
        System.out.println("0: max FwDeg/dom (default)");
        System.out.println("1: max Wdeg/dom, FwDegDom Value Ordering");
        System.out.println("2: Jeroslow-Wang for CSP, FwDegDom Value Ordering");
        System.out.println("3: MOM for CSP, FwDegDom Value Ordering");
        System.out.println("4: EWdeg, FwDegDom Value Ordering");
        System.out.println("5: DLCS for CSP, FwDegDom Value Ordering");
        System.out.println("7: JW Var Ordering, JW Value Ordering");
        System.out.println("8: FwDeg/dom Var Ordering, JW Value Ordering");
        System.out.println("9: FwDeg/dom Var Ordering, DLCS Value Ordering");
        System.out.println("10: JW Var Ordering, DLCS Value Ordering");

        System.out.println("\n\n");
    }

}
