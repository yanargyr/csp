package csp.solvers;


import csp.data.SolverResult;
import csp.enums.ProblemState;

import csp.filtering.Filtering;
import csp.filtering.GAC;
import csp.heuristics.HeuristicFactory;
import csp.problems.Problem;

import java.util.Arrays;


public class MACSolver extends Solver {


    public MACSolver(Problem p) {
        super(p);
    }

    //@Override
    public SolverResult solve2(int heuristic_to_run, int timeLimitSeconds) {
        this.timeLimit = timeLimitSeconds;


        heuristicVariableOrdering = HeuristicFactory.getVariableOrdering(heuristic_to_run);
        heuristicValueOrdering = HeuristicFactory.getValueOrdering(heuristic_to_run);


        if (consoleDebugging) {
            printContraints(false);
        }

        SolverResult result = new SolverResult();
        result.vars = this.p.totalVars;
        result.domain = this.p.domainSize;
        result.constraints = p.C.length;
        result.heuristicUsed = heuristic_to_run;

        //Filtering filtering = new AC3(this);
        Filtering filtering = new GAC(this);

        heuristicVariableOrdering = HeuristicFactory.getVariableOrdering(heuristic_to_run);
        heuristicValueOrdering = HeuristicFactory.getValueOrdering(heuristic_to_run);

        depth = -1;
        maxDepth = 0;
        problemState = ProblemState.PENDING;
        timedOut = false;
        long startTime, finishTime;

        startTime = System.currentTimeMillis();

        boolean isConsistent = filtering.filter();
        stopNow = false; // will be true if we reach max depth with consistency (Solved), OR if we have to backtrack and are on depth 0 (UnSat)
        updateDynamicArities();

        this.initialDynDom = Arrays.copyOf(dynDom, dynDom.length);


        while (!stopNow) {

            if (consoleDebugging) {
                printAvailables();
                printContraints(true);
            }

            //z++;
            if (consoleDebugging) {
                System.out.print("\n>\n>>\n>>> \n>>>>\n ------------------new round ( move:" + nodes + ", depth:" + depth + " " + stopNow + ") --------------- " + depth + "\n>>>>\n>>>\n>>\n>");
            }
            if (depth > maxDepth) {
                maxDepth = depth;
            }

            updateDynamicDomains();
            updateDynamicArities();

            if (isConsistent) { // TODO : also check isSound here
                if (depth == p.totalVars - 1) {
                    if (isSound()) {
                        problemState = ProblemState.SOLVED;
                        stopNow = true;
                    } else {
                        backtrack();
                    }
                } else {
                    depth++;

                    heuristicVariableOrdering.orderVariables(this);
                    this.selectWinnerVariableForNextBranch();
                    heuristicValueOrdering.orderValues(this);

                    markAndSkipUnsupportedValues();
                    selectValueForNextBranch();

                    if (consoleDebugging) {
                        this.printOrderings();
                    }
                    this.assign(this.winnerVar, this.winnerVal);
                }

            } else {
                if (depth >= 0) {
                    backtrack();
                } else {
                    stopNow = true;
                    problemState = ProblemState.INCONSISTENT;
                }
            }

            isConsistent = filtering.filter();
            updateDynamicDomains();

            if (consoleDebugging) {
                printLog();
                printAvailables();
                printDynamicDomains();
            }

            finishTime = System.currentTimeMillis();
            long timeNow = (long) ((finishTime - startTime) / 999f);
            if ((int) timeNow > this.timeLimit) {
                timedOut = true;
                problemState = ProblemState.TIMEDOUT;
                this.stopNow = true;
            }
        }

        if (problemState == ProblemState.SOLVED) {
            result.assignments = Arrays.copyOf(this.assignments, this.assignments.length);
        }

        finishTime = System.currentTimeMillis();
        timeTotal = (finishTime - startTime) / 999f;


        result.problemStatus = this.problemState;
        result.timeSpent = this.timeTotal;
        result.backtracks = this.backtracks;
        result.nodes = this.nodes;
        result.maxDepth = this.maxDepth;

        return result;

    }

    @Override
    public SolverResult solve(int heuristic_to_run, int timeLimitSeconds) {


        //////////// Initialize For Problem Solution /////////
        this.timeLimit = timeLimitSeconds;

        heuristicVariableOrdering = HeuristicFactory.getVariableOrdering(heuristic_to_run);
        heuristicValueOrdering = HeuristicFactory.getValueOrdering(heuristic_to_run);


        if (consoleDebugging) {
            printContraints(false);
        }

        SolverResult result = new SolverResult();
        result.vars = this.p.totalVars;
        result.domain = this.p.domainSize;
        result.constraints = p.C.length;
        result.heuristicUsed = heuristic_to_run;

        //Filtering filtering = new AC3(this);
        Filtering filtering = new GAC(this);


        depth = -1;
        maxDepth = 0;
        problemState = ProblemState.PENDING;
        timedOut = false;
        long startTime, finishTime;

        startTime = System.currentTimeMillis();

        boolean isConsistent = filtering.filter();
        stopNow = false; // will be true if we reach max depth with consistency (Solved), OR if we have to backtrack and are on depth 0 (UnSat)
        updateDynamicArities();

        this.initialDynDom = Arrays.copyOf(dynDom, dynDom.length);


        //////////// Find Solution /////////

        while (!stopNow) {

            if (consoleDebugging) {
                printAvailables();
                printContraints(true);
            }

            if (consoleDebugging) {
                System.out.print("\n>\n>>\n>>> \n>>>>\n ------------------new round ( move:" + nodes + ", depth:" + depth + " " + stopNow + ") --------------- " + depth + "\n>>>>\n>>>\n>>\n>");
            }
            if (depth > maxDepth) {
                maxDepth = depth;
            }

            updateDynamicDomains();
            updateDynamicArities();

            if (isConsistent) {
                if (depth == p.totalVars - 1) {
                    if (isSound()) {
                        problemState = ProblemState.SOLVED;
                        stopNow = true;
                    } else {
                        backtrack();
                    }
                } else {
                    depth++;

                    heuristicVariableOrdering.orderVariables(this);
                    this.selectWinnerVariableForNextBranch();
                    heuristicValueOrdering.orderValues(this);

                    markAndSkipUnsupportedValues();
                    selectValueForNextBranch();

                    if (consoleDebugging) {
                        this.printOrderings();
                    }
                    this.assign(this.winnerVar, this.winnerVal);
                }

            } else {
                if (depth >= 0) {
                    backtrack();
                } else {
                    stopNow = true;
                    problemState = ProblemState.INCONSISTENT;
                }
            }

            isConsistent = filtering.filter();
            updateDynamicDomains();

            if (consoleDebugging) {
                printLog();
                printAvailables();
                printDynamicDomains();
            }

            finishTime = System.currentTimeMillis();
            long timeNow = (long) ((finishTime - startTime) / 999f);
            if ((int) timeNow > this.timeLimit) {
                timedOut = true;
                problemState = ProblemState.TIMEDOUT;
                this.stopNow = true;
            }
        }


        //////////// Export Results /////////

        if (problemState == ProblemState.SOLVED) {
            result.assignments = Arrays.copyOf(this.assignments, this.assignments.length);
        }

        finishTime = System.currentTimeMillis();
        timeTotal = (finishTime - startTime) / 999f;


        result.problemStatus = this.problemState;
        result.timeSpent = this.timeTotal;
        result.backtracks = this.backtracks;
        result.nodes = this.nodes;
        result.maxDepth = this.maxDepth;

        return result;

    }

    @Override
    public void assign(int var, int val) {
        if (consoleDebugging) {
            System.out.print("\n\n################### Branching : x" + var + " = " + val);
        }

        this.nodes++;
        this.branchHistory[depth][p.domainSize] = var;
        this.branchHistory[depth][val] = 1;

        this.currentBranchVar = var;
        this.currentBranchValue = val;
        this.assignments[var] = val;

        // Removing support for other values of that variable
        for (int v = 0; v < p.domainSize; v++) {
            if (v != this.currentBranchValue && this.hasSupport(this.currentBranchVar, v)) {
                this.markAsUnsupported(currentBranchVar, v, currentBranchVar, currentBranchValue);
            }
        }
    }

    @Override
    protected void backtrack() {
        int Rvar = this.branchHistory[depth][p.domainSize];
        this.assignments[Rvar] = -1;

        for (int i = 0; i < p.totalVars; i++) {
            for (int j = 0; j < p.domainSize; j++) {
                // Unmark values that were lost support from last assignment
                if (this.supportStatus[i][j][0] == Rvar) {
                    this.markAsSupported(i, j);
                }
            }
        }


        // If we have no more values to try, we should backtrack to previous level
        if (this.triedAllValuesAtDepth(depth)) {
            // Except we are at depth 0, so there is nothing else to try. The problem can't be solved
            if (depth == 0) {
                //--System.out.print("\n\n >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>Problem can't be solved \n\n\n");
                this.stopNow = true;
                problemState = ProblemState.UNSAT;
            } else {
                // Backtracking to previous level
                if (consoleDebugging) {
                    System.out.print("\n\n <--------------------- Backtracking to previous depth \n\n\n");
                }
                this.backtracks++;

                // Deleting the assignment at the failed depth
                for (int j = 0; j <= p.domainSize; j++) {
                    this.branchHistory[depth][j] = -1;
                }

                depth--;

                // Lets try another value at the old depth
                this.winnerVar = this.branchHistory[depth][p.domainSize];
                updateDynamicArities();

                heuristicValueOrdering.orderValues(this);

                backtrack();
            }
        } else {
            // We are here because we had a failed assignment, but there are more values left to assign
            // We have to run the value ordering again
            if (consoleDebugging) System.out.print("\n\n >>>>>>>>>>>>>>>>>>>>>>> Trying another value \n\n\n");

            // Selecting the next value to try
            this.selectValueForNextBranch();

            //branching the new value
            if (consoleDebugging) this.printOrderings();
            this.assign(this.winnerVar, this.winnerVal);
        }
    }
}
