package csp.solvers;

import csp.problems.Problem;

public class SolverFactory {

    public static Solver getDefaultsolver(final Problem p) {
        return new MACSolver(p);
    }
}
