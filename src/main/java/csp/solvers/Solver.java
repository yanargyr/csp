package csp.solvers;

import csp.constraints.Constraint;
import csp.data.SolverResult;
import csp.enums.ProblemState;
import csp.exceptions.CSPException;
import csp.heuristics.valueOrdering.ValueOrdering;
import csp.heuristics.variableOrdering.VariableOrdering;
import csp.problems.Problem;

import static csp.enums.ValueSupports.SUPPORTED;
import static csp.enums.ValueSupports.UNSUPPORTED_INITIALLY;

public class Solver {
    public Problem p;


    /**
     * the current variable assignments
     * assignments[4] = 3 means x4 is assigned with value 3
     * assignments[7] = -1 means x7 is unassigned
     */
    public int assignments[];

    /**
     * Stores support status for each variable/ value combination
     * <p>
     * Possible states:
     * - SUPPORTED (-1): There is at least one support for this assignent
     * - UNSUPPORTED_INITIALLY(-2):  Value was wiped from an Arc Consistency filter any assignment took place
     * - UNSUPPORTED (>=0): Value lost support because of another assignment
     * <p>
     * size = arity * domainSize * 2
     * <p>
     * Example, if x1 cannot have the value 2 (not supported)
     * because of the assignment x3=4, then:
     * supportStatus[1][2][0] = 3
     * supportStatus[1][2][1] = 4
     * <p>
     * if supportStatus[i][j][0] = supportStatus[i][j][1] == -1
     * then xi=j is still supported
     * <p>
     * if supportStatus[i][j][0] = supportStatus[i][j][1] == -2
     * then xi=j was not supported before any assignment
     */
    protected int supportStatus[][][];      // wiped out Values, and the assignment that caused the wipe out.Used for backtracking


    public int depth;                  // the current depth of our search tree

    /**
     * Stores the values we tried to assign / branch
     * Size : depth * domainSize + 1 (value ids + the var id)
     * 
     * Example :
     * domainSize size: 5
     * branchHistory[1][5] = 3 -> in depth 1 we are assigning values to var x3
     * branchHistory[24][2] = 1  AND branchHistory[24][5] = 8 -> In depth 24, we have tried setting x8=2
     * branchHistory[15][4] = -1 AND branchHistory[15][5] = 6 -> In depth 15, we haven't tried setting x6=4 yet
     */
    public int branchHistory[][];      //




    // The ID of the var that was selected in current branch
    public int currentBranchVar;

    // The value that is now assigned to currentBranchVar
    public int currentBranchValue;

    // This will contain the scores of the variables given by the HeuristicVarOrdering for Variable ordering
    public double scoresVarOrdering[];

    // This will contain the scores for the values, as estimated by the Value ordering of the heuristic.
    public double scoresValueOrdering[];

    // This will be the Variable with the maximum score, according to the Variable ordering
    public int winnerVar;

    // This will be the Value with the maximum score, according to the Value ordering
    public int winnerVal;

    // The current domain size of each variable, including values wiped out after Arc Consistency checks
    public int dynDom[];

    // The initial domain size of each variable, after a first Arc consistency check, but before any variable assignments
    public int initialDynDom[];

    // Dynamic arity of each constraint (How many unassigned variables involved in each constraint)
    public int dynArity[];

    public boolean stopNow;

    public int timeLimit;
    public ProblemState problemState;
    public boolean timedOut;
    public int maxDepth;
    public float timeTotal;

    public int backtracks;

    public int nodes;

    // print solver events in console
    protected boolean consoleDebugging = false;

    public ValueOrdering heuristicValueOrdering;
    public VariableOrdering heuristicVariableOrdering;

    // Counting occurrences where a constraint causes a domain wipe out. Can be used for weighted degree heuristics
    public int[] constraintConflicts;

    public Solver (final Problem p) {

        this.p = p;
        this.dynArity = new int[p.totalConstraints];

        this.constraintConflicts = new int[p.totalConstraints];

        this.backtracks = 0;
        this.timeLimit = timeLimit;
        this.assignments = new int[p.totalVars];
        this.supportStatus = new int[p.totalVars][p.domainSize][2];
        this.branchHistory = new int[p.totalVars][p.domainSize + 1];

        this.scoresVarOrdering = new double[p.totalVars];
        this.scoresValueOrdering = new double[p.domainSize];
        this.dynDom = new int[p.totalVars];
        this.initialDynDom = new int[p.totalVars];

        for (int i = 0; i < p.totalVars; i++) {
            this.dynDom[i] = p.domainSize;
        }

        this.depth = 0;
        this.winnerVar = -1;
        this.winnerVal = -1;

        this.currentBranchVar = -2;
        this.currentBranchValue = -2;

        for (int i = 0; i < p.totalVars; i++) {
            assignments[i] = -1;
        }

        // leaving the assign log blank
        for (int i = 0; i < p.totalVars; i++) {
            for (int j = 0; j <= p.domainSize; j++) {
                branchHistory[i][j] = -1;
            }
        }

        for (int i = 0; i < p.totalVars; i++) {
            for (int j = 0; j < p.domainSize; j++) {
                markAsSupported(i, j);
            }
        }


    }

    public SolverResult solve(final int heuristic_to_run, final int timeLimitSeconds) {
        throw new CSPException("Need to instantiate specific Solver subclass");
    }

    public void assign(int var, int val) {
        throw new CSPException("Need to instantiate specific Solver subclass");
    }

    protected void backtrack() {
        throw new CSPException("Need to instantiate specific Solver subclass");
    }



    protected void selectWinnerVariableForNextBranch() {
        /*
         * Used for branching in next depth
         * This function searches the Variable and Value Ordering scores and finds and stores the winner variable and value
         */
        int var;
        double varScore;

        var = -1;
        varScore = 0;

        for (int i = 0; i < this.p.totalVars; i++) {
            if (this.scoresVarOrdering[i] >= varScore && (!isVarTriedInAnyDepth(i) || (!triedAllValuesAtDepth(this.depth) && this.branchHistory[depth][p.domainSize] == i))) {
                var = i;
                varScore = scoresVarOrdering[i];
            }
        }
        this.winnerVar = var;
    }


    protected void selectValueForNextBranch() {
        int val;
        double valScore;

        val = -1;

        valScore = 0;
        for (int j = 0; j < p.domainSize; j++) {
            if (this.scoresValueOrdering[j] >= valScore && !wasAssigmentTriedAlready(winnerVar, j)) {
                val = j;
                valScore = scoresValueOrdering[j];
            }
        }
        this.winnerVal = val;
    }

    /**
     * Returns whether a variable was/is already assigned in a branch
     *
     * @param varGlobalId
     * @return
     */
    public boolean isVarTriedInAnyDepth(int varGlobalId) {
        boolean assigned = false;
        for (int i = 0; i < p.totalVars; i++) {
            if (this.branchHistory[i][p.domainSize] == varGlobalId) {
                assigned = true;
            }
        }
        return (assigned);
    }

    protected boolean wasAssigmentTriedAlready(int var, int val) {
        for (int i = 0; i < p.totalVars; i++) {
            if (this.branchHistory[i][p.domainSize] == var && this.branchHistory[i][val] != -1 || this.branchHistory[depth][val] != -1)
                return true;
        }
        return false;
    }

    protected void printDynamicDomains() {
        for (int i = 0; i < this.p.totalVars; i++)
            System.out.print("\n dom(x" + i + ")=" + this.dynDom[i]);
    }


    /**
     * Marking supportStatus values as "tried" so we won't try them
     */
    protected void markAndSkipUnsupportedValues() {
        for (int i = 0; i < p.domainSize; i++) {
            if (!this.hasSupport(winnerVar, i)) {
                this.branchHistory[depth][i] = 2;
            }
        }
    }

    /**
     * Counts the dynamic arity of each constraint
     * (How many unassigned variables involved)
     */
    protected void updateDynamicArities() {
        for (int i = 0; i < p.C.length; i++) {
            int v = 0;
            for (int j = 0; j < p.C[i].varGlobalIDs.length; j++) {
                int x = p.C[i].varGlobalIDs[j];
                if (this.assignments[x] == -1) v++;
            }
            this.dynArity[i] = v;
        }
    }

    /**
     * Counting dynamic domainSize for each variable (how many values available after consistency check
     */
    protected void updateDynamicDomains() {
        int k;
        for (int i = 0; i < p.totalVars; i++) {
            k = 0;
            for (int j = 0; j < p.domainSize; j++) {
                if (hasSupport(i, j)) {
                    k++;
                }
            }
            this.dynDom[i] = k;
        }
    }

    protected boolean triedAllValuesAtDepth(int depth) {
        for (int i = 0; i < p.domainSize; i++) {
            if (this.branchHistory[depth][i] == -1) {
                return false;
            }
        }
        return true;
    }

    /**
     * Is problem sound, i.e, are all constraints sat with current assignments
     *
     * @return
     */
    public boolean isSound() {
        for (int c = 0; c < p.C.length; c++) {
            if (!p.C[c].isSatisfied(this.assignments)) {
                return false;
            }
        }
        return true;
    }



    /**
     * Checks if a variable's value has at least one support
     *
     * @param varGlobalId
     * @param value
     * @return
     */
    public boolean hasSupport(final int varGlobalId, final int value) {
        return supportStatus[varGlobalId][value][0] == SUPPORTED;
    }

    /**
     * Marks that x[varGlobalId] = value has at least one support
     *
     * @param varGlobalId
     * @param value
     */
    public void markAsSupported(final int varGlobalId, final int value) {
        supportStatus[varGlobalId][value][0] = SUPPORTED;
        supportStatus[varGlobalId][value][1] = SUPPORTED;
    }

    /**
     * Marks that x[varGlobalId] was not supported, before any variable was assigned any value
     *
     * @param varGlobalId
     * @param value
     */
    public void markAsInitiallyUnsupported(final int varGlobalId, final int value) {
        supportStatus[varGlobalId][value][0] = UNSUPPORTED_INITIALLY;
        supportStatus[varGlobalId][value][1] = UNSUPPORTED_INITIALLY;
    }



    /**
     * Marks that x[varGlobalId_1] = value_1 has lost support, as result of
     * the assignment x[varGlobalId_2] = value_2
     *
     * @param varGlobalId_1
     * @param value_1
     * @param varGlobalId_2
     * @param value_2
     */
    public void markAsUnsupported(final int varGlobalId_1, final int value_1, final int varGlobalId_2, final int value_2) {
        supportStatus[varGlobalId_1][value_1][0] = varGlobalId_2;
        supportStatus[varGlobalId_1][value_1][1] = value_2;
    }


    //////////////////////////////
    // Printing methods
    ///////////////////////////////

    protected void printLog() {
        System.out.print("\n ######### Printing csp.Log ############# \n");

        for (int i = 0; i < p.domainSize; i++) {
            System.out.print(" " + i);
        }

        for (int i = 0; i < p.totalVars; i++) {
            System.out.print("\n");
            for (int j = 0; j < p.domainSize; j++) {
                if (branchHistory[i][j] == -1) System.out.print(" x");
                else System.out.print(" " + branchHistory[i][j]);
            }
            System.out.print(" <-- x" + branchHistory[i][p.domainSize]);
        }
    }

    protected void printAvailables() {
        System.out.print("\n######### Printing domains #############\n  ");
        for (int j = 0; j < p.totalVars; j++) {

            System.out.print(" x" + j);
        }
        for (int i = 0; i < p.domainSize; i++) {
            System.out.print("\n" + i + ":");
            for (int j = 0; j < p.totalVars; j++) {
                if (hasSupport(j, i)) {
                    System.out.print(" v ");
                } else {
                    int v = supportStatus[j][i][0];
                    if (v == -2) {
                        System.out.print(" X ");
                    } else {
                        System.out.print(" " + v + " ");
                    }

                }
            }
        }
    }

    protected void printOrderings() {
        System.out.print("\n >>>>>>>>>>>>>> Printing Variable Ordering <<<<<<<<<<<\n");
        for (int i = 0; i < this.p.totalVars; i++) {
            System.out.print("\n x" + i + " --> " + this.scoresVarOrdering[i]);
            if (i == this.winnerVar) System.out.print("<--");

        }

        System.out.print("\n >>>>>>>>>>>>>>> Printing Value Ordering <<<<<<<<<<<<<< \n");
        for (int j = 0; j < this.p.domainSize; j++) {
            System.out.print("\n x" + winnerVar + " = " + j + " -->" + this.scoresValueOrdering[j]);
            if (j == this.winnerVal) System.out.print("<--");
            if (this.branchHistory[depth][j] != -1) System.out.print(" X " + this.branchHistory[depth][j]);
        }
        System.out.print("\n current winner is : x" + this.winnerVar + "=" + this.winnerVal);
    }

    /**
     * Printing the constraints and their dynamic arities
     */
    protected void printContraints(boolean showOnlySupported) {
        System.out.println("\n\n printing constraints");
        for (int i = 0; i < p.C.length; i++) {

            Constraint c = p.C[i];

            System.out.print("\n #" + i + " \t{");
            for (int j = 0; j < c.arity; j++) {
                System.out.print(c.varGlobalIDs[j] + " ");
            }
            System.out.print("}");
            System.out.print(" " + this.dynArity[i]);
            System.out.println();
            for (int t = 0; t < c.tuples.length; t++) {

                StringBuilder ss = new StringBuilder(t + ":\t");

                boolean hasSupport = true;
                for (int var = 0; var < c.arity; var++) {
                    int val = c.getVarValueInTuple(var, t);
                    if (c.countSupportsDynamic(c.varGlobalIDs[var], val, this) == 0)
                        hasSupport = false;
                    ss.append(" " + val);
                }
                ss.append(" | " + c.tuples[t]);

                if (!showOnlySupported || (c.tuples[t] == 1 && hasSupport)) {
                    System.out.println(ss);
                }
            }
        }
    }


}
