package csp.heuristics;

import csp.enums.HeuristicStrategy;
import csp.heuristics.valueOrdering.ValueOrdering;
import csp.heuristics.variableOrdering.*;

public class HeuristicFactory {

    public static VariableOrdering getVariableOrdering(final int heuristic_code) {
        switch (heuristic_code) {
            case HeuristicStrategy.FWDEGDOM: {
                return new FwDegDom();
            }
            case HeuristicStrategy.WDEG_FWDEGDOM: {
                return new WdegDom();
            }
            case HeuristicStrategy.JW_FWDEGDOM: {
                return new JW2s_CSPv4();
            }
            case HeuristicStrategy.MOM_FWDEGDOM: {
                return new MOM_CSP();
            }
            case HeuristicStrategy.EWDEG_FWDEGDOM: {
                return new EWdeg();
            }
            case HeuristicStrategy.DLCS_FWDEGDOM: {
                return new DLCS_CSPv3();
            }

            case HeuristicStrategy.JW: {
                return new JW2s_CSPv4();
            }
            case HeuristicStrategy.JW_DLCS: {
                return new JW2s_CSPv4();
            }

            default:
                return new FwDegDom();
        }
    }

    public static ValueOrdering getValueOrdering(int h) {
        switch (h) {
            case HeuristicStrategy.FWDEGDOM: {
                return new csp.heuristics.valueOrdering.FwDegDom();
            }
            case HeuristicStrategy.JW: {
                return new csp.heuristics.valueOrdering.JW2s_CSPv4();
            }
            case HeuristicStrategy.FWDEGDOM_JW: {
                return new csp.heuristics.valueOrdering.JW2s_CSPv4();
            }
            case HeuristicStrategy.FWDEGDOM_DLCS: {
                return new csp.heuristics.valueOrdering.DLCS_CSPv3();
            }
            case HeuristicStrategy.JW_DLCS: {
                return new csp.heuristics.valueOrdering.DLCS_CSPv3();
            }

            default:
                return new csp.heuristics.valueOrdering.FwDegDom();
        }
    }

    public static String getHeuristicDescription(int h) {
        String heuristicString;
        switch (h) {
            case 0: {
                heuristicString = "max forward Deg / dom";
                break;
            }
            case 1: {
                heuristicString = "wdeg/dom var ordering, FwDegDom Value Ordering";
                break;
            }
            case 2: {
                heuristicString = "Jeroslow-Wang var ordering, FwDegDom Value Ordering";
                break;
            }
            case 3: {
                heuristicString = "MOM var ordering, FwDegDom Value Ordering";
                break;
            }
            case 4: {
                heuristicString = "EWdeg var ordering, FwDegDom Value Ordering";
                break;
            }
            case 5: {
                heuristicString = "DLCS var ordering, FwDegDom Value Ordering";
                break;
            }

            case 7: {
                heuristicString = "JW Var Ordering, JW Value Ordering";
                break;
            }
            case 8: {
                heuristicString = "FwDeg/dom Var Ordering, JW Value Ordering";
                break;
            }
            case 9: {
                heuristicString = "FwDeg/dom Var Ordering, DLCS Value Ordering";
                break;
            }
            case 10: {
                heuristicString = "JW Var Ordering, DLCS Value Ordering";
                break;
            }
            default:
                heuristicString = "max forward Deg / dom";
        }
        return heuristicString;
    }
}
