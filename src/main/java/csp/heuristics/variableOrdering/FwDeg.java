package csp.heuristics.variableOrdering;

import csp.solvers.Solver;
import csp.problems.Problem;

public class FwDeg implements VariableOrdering {

    @Override
    public void orderVariables(Solver s) {
        Problem p = s.p;
        for (int i = 0; i < p.totalVars; i++) {
            int degree = 0;
            for (int j = 0; j < p.C.length; j++) {
                if (p.C[j].containsVar(i) && s.dynArity[j] > 0) degree++;
            }
            s.scoresVarOrdering[i] = (double) degree;
        }
    }
}
