package csp.heuristics.variableOrdering;

import csp.solvers.Solver;

public interface VariableOrdering {
    void orderVariables(Solver s);
}
