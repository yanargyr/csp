package csp.heuristics.variableOrdering;

import csp.solvers.Solver;
import csp.problems.Problem;

public class JW2s_CSPv4 implements VariableOrdering {
    @Override
    public void orderVariables(Solver s) {
        Problem p = s.p;
        double a = 2;

        // Zeroing down all variable scores
        for (int i = 0; i < p.totalVars; i++) {
            s.scoresVarOrdering[i] = 0;
        }

        // iterating all the constrains
        for (int c = 0; c < p.C.length; c++) {
            // We are only interested in constraints with at least two unassigned variables
            if (s.dynArity[c] > 1) {
                double ar = (double) s.dynArity[c];

                //Giving more weight to vars involved in constraints of smaller arities, as these are more likely to cause wipeouts
                double factor = Math.pow(a, -ar);

                //for each variable in the constraint
                for (int j = 0; j < p.C[c].varGlobalIDs.length; j++) {
                    int x = p.C[c].varGlobalIDs[j];

                    if (!s.isVarTriedInAnyDepth(x)) {
                        s.scoresVarOrdering[x] += factor;
                    }
                }
            }
        }

        // Will prefer vars with smaller dynamic domainSize, as this will limit the next branches
        for (int i = 0; i < p.totalVars; i++) {
            if (!s.isVarTriedInAnyDepth(i))
                s.scoresVarOrdering[i] /= s.dynDom[i];
        }
    }
}
