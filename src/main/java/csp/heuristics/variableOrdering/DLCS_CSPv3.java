package csp.heuristics.variableOrdering;

import csp.solvers.Solver;
import csp.problems.Problem;

public class DLCS_CSPv3 implements VariableOrdering{
    @Override
    public void orderVariables(Solver s) {
        Problem p = s.p;
        for (int i = 0; i < p.totalVars; i++) {
            double score = 0;
            for (int j = 0; j < p.domainSize; j++) {
                for (int c = 0; c < p.C.length; c++) {
                    if (p.C[c].containsVar(i)) {
                        score += p.C[c].countSupportsDynamic(i, j, s);
                    }
                }

            }

            // maximize
            s.scoresVarOrdering[i] = score / (Math.pow(s.dynDom[i], 2));
            // minimize
            // TODO try with minimise
            // P.scoresVarOrdering[i] = 1.0/score;//*(Math.pow(P.dynDom[i],2));
        }
    }
}

