package csp.heuristics.variableOrdering;

import csp.solvers.Solver;
import csp.problems.Problem;

public class FwDegDom implements VariableOrdering{

    @Override
    public void orderVariables(Solver s) {
        Problem p = s.p;
        /*
         *this is the classic fdeg/dom algorithm
         *It counts the constraints that involve the variable and at least one more uassigned var
         */
        for (int i = 0; i < p.totalVars; i++) {
            s.scoresVarOrdering[i] = 0;
        }

        for (int c = 0; c < p.C.length; c++) {
            if (s.dynArity[c] > 1) {
                for (int i = 0; i < p.C[c].arity; i++) {
                    int x = p.C[c].varGlobalIDs[i];
                    if (!s.isVarTriedInAnyDepth(x)) {
                        s.scoresVarOrdering[x]++;
                    }
                }
            }
        }

        for (int i = 0; i < p.totalVars; i++) {
            s.scoresVarOrdering[i] /= s.dynDom[i];
        }
    }
}
