package csp.heuristics.variableOrdering;

import csp.solvers.Solver;
import csp.constraints.Constraint;
import csp.problems.Problem;

public class EWdeg implements VariableOrdering {

    @Override
    public void orderVariables(Solver s) {
        Problem p = s.p;

        // Zeroing down all variable scores
        for (int i = 0; i < p.totalVars; i++) {
            s.scoresVarOrdering[i] = 0;
        }

        // Calculate Constraint Weights
        double arityWeight = 2.0;
        double[] constraintWeights = new double[p.totalConstraints];
        for (int c = 0; c < p.totalConstraints; c++) {

            Constraint con = p.C[c];
            if (s.dynArity[c] > 1) {

                // Weight 1 will prefer constraints that cause wipeouts more often. At first depths will not have much impact, but it become more significant later on
                double w1 = 1 + s.constraintConflicts[c];

                // Weight 2 will prefer constraints with smaller dynamic arity, hoping to propagate conflicts as early as possible. Will work similarly to FwDegDom at first depths
                double w2 = Math.pow(arityWeight, -s.dynArity[c]);

                // Weight 3 prefers constraints that involves vars with lower dynamic domain
                double w3 = 1;
                for (int v = 0; v < con.arity; v++) {
                    if (s.assignments[v] == -1) {
                        w3 = w3 * (double) s.initialDynDom[v] / (double) s.dynDom[v];
                    }
                }

                constraintWeights[c] = w1 * w2 * w3;

                for (int v = 0; v < con.arity; v++) {
                    int globalId = con.varGlobalIDs[v];
                    if (!s.isVarTriedInAnyDepth(globalId)) {
                        s.scoresVarOrdering[globalId] += constraintWeights[c];
                    }
                }
            }

        }


        // Will prefer vars with smaller dynamic domain, as this will limit the next branches
        for (int i = 0; i < p.totalVars; i++) {
            if (!s.isVarTriedInAnyDepth(i)) {
                s.scoresVarOrdering[i] /= s.dynDom[i];
            }
        }


    }
}
