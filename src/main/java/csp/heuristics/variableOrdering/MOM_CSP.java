package csp.heuristics.variableOrdering;

import csp.solvers.Solver;
import csp.problems.Problem;

public class MOM_CSP implements VariableOrdering {
    @Override
    public void orderVariables(Solver s) {
        Problem p = s.p;
        //finding the constraint of minimum arity in the problem
        int minarity = s.dynArity[0];
        for (int c = 0; c < p.C.length; c++) {
            if (s.dynArity[c] < minarity && s.dynArity[c] > 1) {
                minarity = s.dynArity[c];
            }
        }

        for (int i = 0; i < p.totalVars; i++) {
            s.scoresVarOrdering[i] = 0;
        }

        /*
         *Counting variable appearances in constraints of minimum arity
         */

        for (int c = 0; c < p.C.length; c++) {
            //if(P.C[c].arity == minarity)
            if (s.dynArity[c] == minarity) {
                for (int i = 0; i < p.C[c].varGlobalIDs.length; i++) {
                    //int x = P.C[c].varGlobalIDs[i];
                    // if(!P.isVarTriedInAnyDepth(i))
                    if (!s.isVarTriedInAnyDepth(p.C[c].varGlobalIDs[i]))
                        s.scoresVarOrdering[p.C[c].varGlobalIDs[i]]++;
                }
            }
        }

        for (int i = 0; i < p.totalVars; i++) {
            if (!s.isVarTriedInAnyDepth(i)) {
                s.scoresVarOrdering[i] = s.scoresVarOrdering[i] / (double) s.dynDom[i];
            } else {
                s.scoresVarOrdering[i] = 0;
            }
        }

        //P.printOrderings();

        /*
         *Finding ties if any..
         */
        double maxscore = 0;
        int maxScoreDuplicates = 1;
        for (int i = 0; i < p.totalVars; i++) {
            if (!s.isVarTriedInAnyDepth(i)) {
                if (s.scoresVarOrdering[i] == maxscore) {
                    maxScoreDuplicates++;
                }
                if (s.scoresVarOrdering[i] > maxscore) {
                    maxScoreDuplicates = 1;
                    maxscore = s.scoresVarOrdering[i];
                }
            }
        }

        /*any ties ??
         */
        //--  BreakTies
        //maxScoreDuplicates = 0;
        if (maxScoreDuplicates > 1) {
            //System.out.print("\n found " + maxScoreDuplicates + " ties with score " + maxscore);
            /*
             *Counting value supports for tieing variables
             **/
            for (int i = 0; i < p.totalVars; i++) {
                //choosing only tieing totalVars
                if (s.scoresVarOrdering[i] == maxscore && !s.isVarTriedInAnyDepth(i)) {
                    double sups[] = new double[p.domainSize];

                    //counting supports on constraints of minimum arity
                    for (int c = 0; c < p.C.length; c++) {
                        if (s.dynArity[c] == minarity && p.C[c].containsVar(i)) {
                            double ar = (double) s.dynArity[c];
                            double factor = Math.pow(1, -ar);

                            for (int j = 0; j < p.domainSize; j++) {
                                if (s.hasSupport(i, j))
                                    sups[j] += factor * (double) (p.C[c].countSupportsDynamic(i, j, s));
                            }
                        }
                    }
                    double supSum = 0.0;
                    for (int j = 0; j < p.domainSize; j++) {
                        if (s.hasSupport(i, j)) {
                            supSum += sups[j];
                        }
                    }
                    s.scoresVarOrdering[i] += 1.0 / (s.dynDom[i] / supSum);
                }
            }
        }
    }
}
