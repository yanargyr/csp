package csp.heuristics.variableOrdering;

import csp.solvers.Solver;
import csp.constraints.Constraint;
import csp.problems.Problem;

/**
 * Implementation of dom/wdeg algorithm
 * Frederic Boussemart, Fred Hemery, Christophe Lecoutre, Lakhdar Sais
 *
 */
public class WdegDom implements VariableOrdering {

    @Override
    public void orderVariables(Solver s) {
        Problem p = s.p;


        // Zeroing down all variable scores
        for (int i = 0; i < p.totalVars; i++) {
            s.scoresVarOrdering[i] = 0;
        }

        // Calculate Constraint Weights
        double[] constraintWeights = new double[p.totalConstraints];
        for (int c = 0; c < p.totalConstraints; c++) {

            Constraint con = p.C[c];
            if (s.dynArity[c] > 1) {

                // Weight 1 will prefer constraints that cause wipeouts more often. At first depths will not have much impact, but it become more significant later on
                double w1 = 1 + s.constraintConflicts[c];

                // Weight 2 prefers constraints that involves vars with lower dynamic domain
                double w2 = 1;
                for (int v = 0; v < con.arity; v++) {
                    if (s.assignments[v] == -1) {
                        w2 = w2 * (double) s.initialDynDom[v] / (double) s.dynDom[v];
                    }
                }

                constraintWeights[c] = w1 * w2;

                for (int v = 0; v < con.arity; v++) {
                    int globalId = con.varGlobalIDs[v];
                    if (!s.isVarTriedInAnyDepth(globalId)) {
                        s.scoresVarOrdering[globalId] += constraintWeights[c];
                    }
                }
            }

        }


        // Will prefer vars with smaller dynamic domain, as this will limit the next branches
        for (int i = 0; i < p.totalVars; i++) {
            if (!s.isVarTriedInAnyDepth(i)) {
                s.scoresVarOrdering[i] /= s.dynDom[i];
            }
        }


    }
}
