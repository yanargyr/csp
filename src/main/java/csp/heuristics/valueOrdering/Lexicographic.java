package csp.heuristics.valueOrdering;

import csp.solvers.Solver;
import csp.problems.Problem;

public class Lexicographic implements ValueOrdering {
    @Override
    public void orderValues(Solver s) {
        Problem p = s.p;
        for (int i = 0; i < p.domainSize; i++) {
            s.scoresValueOrdering[i] = i;
        }
    }
}
