package csp.heuristics.valueOrdering;

import csp.solvers.Solver;
import csp.problems.Problem;

public class FwDegDom implements ValueOrdering {
    @Override
    public void orderValues(Solver s) {
        Problem p = s.p;
        for (int j = 0; j < p.domainSize; j++) {
            s.scoresValueOrdering[j] = j;//rnd.nextDouble();
        }
    }
}
