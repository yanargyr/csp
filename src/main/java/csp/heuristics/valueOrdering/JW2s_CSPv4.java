package csp.heuristics.valueOrdering;

import csp.solvers.Solver;
import csp.constraints.Constraint;
import csp.problems.Problem;

public class JW2s_CSPv4 implements ValueOrdering {
    @Override
    public void orderValues(Solver s) {
        Problem p = s.p;
        double a = 2;

        for (int j = 0; j < p.domainSize; j++) {
            s.scoresValueOrdering[j] = 0;
        }

        for (int itc = 0; itc < p.C.length; itc++) {
            Constraint c = p.C[itc];

            double ar = (double) s.dynArity[itc];
            double factor = Math.pow(a, -ar);

            for (int j = 0; j < p.domainSize; j++) {
                if (c.containsVar(s.winnerVar)) {
                    double supports = p.C[itc].countSupportsDynamic(s.winnerVar, j, s);//(double)P.C[c].supports[x][j];
                    if (supports > 0) {
                        s.scoresValueOrdering[j] += 1 / (supports * factor);
                    }
                }
            }
        }
    }
}
