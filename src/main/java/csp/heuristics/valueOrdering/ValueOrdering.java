package csp.heuristics.valueOrdering;

import csp.solvers.Solver;

public interface ValueOrdering{
    void orderValues(Solver s);
}
