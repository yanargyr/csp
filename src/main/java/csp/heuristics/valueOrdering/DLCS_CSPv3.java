package csp.heuristics.valueOrdering;

import csp.solvers.Solver;
import csp.problems.Problem;

public class DLCS_CSPv3 implements ValueOrdering {
    @Override
    public void orderValues(Solver s) {
        Problem p = s.p;
        int i = s.winnerVar;
        for (int j = 0; j < p.domainSize; j++) {
            double score = 0;
            for (int c = 0; c < p.C.length; c++) {
                if (p.C[c].containsVar(i)) {
                    /*
                    int k = P.C[c].getLocalIdForVariable(i);
                    if(P.C[c].supports[k][j]>0 )
                    score +=P.C[c].supports[k][j];
                    */
                    score += p.C[c].countSupportsDynamic(i, j, s);
                }
            }
            s.scoresValueOrdering[j] = score;
        }
    }
}
