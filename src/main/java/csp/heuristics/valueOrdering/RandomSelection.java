package csp.heuristics.valueOrdering;

import csp.solvers.Solver;
import csp.problems.Problem;

import java.util.Random;

public class RandomSelection implements ValueOrdering {
    @Override
    public void orderValues(Solver s) {
        Problem p = s.p;
        Random rnd = new Random(System.currentTimeMillis());
        for (int j = 0; j < p.domainSize; j++) {
            s.scoresValueOrdering[j] = rnd.nextDouble();
        }
    }
}
