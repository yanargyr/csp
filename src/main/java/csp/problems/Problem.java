package csp.problems;
/**
 * @author Yannis Argyropoulos
 */

import csp.solvers.Solver;
import csp.constraints.Constraint;
import java.util.List;

public class Problem {
    public Constraint C[];             // the constraint network
    public int totalVars;              // total number of problem variables
    public int totalConstraints;       // total number of non trivial constraints

    public int max_arity;              // maximum (initial) arity of the constraints
    public int min_arity;              // This variable is NOT from Patra ;D


    // The (initial) domainSize size of the variables.
    public int domainSize;


    // print solver events in console
    private boolean consoleDebugging = false;

    public Solver solver;

    public Problem() {
    }

    public Problem(List<Constraint> constraints, int totalVars, int domainSize) {

        this.max_arity = 0;
        this.min_arity = Integer.MAX_VALUE;

        C = new Constraint[constraints.size()];
        for (int i = 0; i < constraints.size(); i++) {
            C[i] = constraints.get(i);
            int arity = C[i].arity;

            if (arity > max_arity) {
                max_arity = arity;
            }
            if (arity < min_arity) {
                this.min_arity = arity;
            }
        }
        this.totalConstraints = constraints.size();
        this.domainSize = domainSize;
        this.totalVars = totalVars;
    }
}