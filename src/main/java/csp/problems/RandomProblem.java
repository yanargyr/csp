package csp.problems;

import csp.util.Util;
import csp.constraints.Constraint;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class RandomProblem extends Problem {

    // If != 0, this will create some variance between each constraint's allowed tuples, so that not all of them are equally tight
    static double constraintTighnessDeviationRatio = 0.2; //

    //p: the probability for a non trivial constraint to appear
    public double p;
    // q: the proportion of tuples that are NOT allowed (q = 'tightness' of constraint, 1-q = 'looseness' of constraint)
    public double q;


    /**
     * Construct a new Random CSP problem
     *
     * @param vars      Number of variables
     * @param min_arity Minimum constraint arity
     * @param max_arity Maximum constraint arity
     * @param p         probability to create a constraint. Bigger means more constraints
     * @param q         constraint tightness. Bigger means tighter (fewer allowed tuples)
     * @param domain    uniform variable domainSize size
     * @param seed      seed for the random algorithm
     */
    public RandomProblem(int vars, int min_arity, int max_arity, double p, double q, int domain, int seed) {

        super();

        this.domainSize = domain;
        this.totalVars = vars;
        this.p = p;
        this.q = q;
        this.min_arity = min_arity;
        this.max_arity = max_arity;

        initRandomConstraints(seed);
    }

    public void initRandomConstraints(int seed) {
        Random rnd = new Random(seed);

        // Calculating total possible constraints, given the number of variables and arities
        int n = totalVars;
        int constraintsToCreate = 0;
        List<Integer> constraintArities = new ArrayList();


        int totalPossibleConstraints = 0;
        for (int k = this.min_arity; k <= this.max_arity; k++) {

            double f = Util.calcLogFact(n) - Util.calcLogFact(k) - Util.calcLogFact(n - k);
            int arityPossibleConstraints = (int) Math.round(Math.exp(f));
            totalPossibleConstraints += arityPossibleConstraints;
            int constraintsForThisArity = (int) (p * arityPossibleConstraints);
            constraintsToCreate += constraintsForThisArity;

            for (int i = 0; i < constraintsForThisArity; i++) {
                constraintArities.add(k);
            }
        }
        Collections.shuffle(constraintArities, rnd);

        //System.out.print("\n >> Creating " + constraintsToCreate + " constraints out of " + totalPossibleConstraints + " total possible ones");
        int constrVarIds[][] = new int[constraintsToCreate][];

        // populating the varIds that will be used inside each random constraint
        for (int c = 0; c < constraintsToCreate; c++) {
            int new_arity = constraintArities.get(c);
            constrVarIds[c] = new int[new_arity];

            //trying to create a unique combinations of varIds: there shouldn't be 2 constraints involving the same variables
            while (true) {
                for (int i = 0; i < new_arity; i++) {
                    constrVarIds[c][i] = rnd.nextInt(totalVars);
                }
                // we are satisfied with a constraint that is unique
                // and doesn't contain one variable twice or more
                if (Util.allRowElementsAreUnique(constrVarIds, c) && Util.isRowUniqueCheckBackwardsOnly(constrVarIds, c))
                    break;
            }
        }

        // Creating the actual constraints Arrays. We will initialise them later
        C = new Constraint[constraintsToCreate];

        // calculating allowed Tuples per constraint
        int totalAllowedTuples = 0;
        int[] constraintTuplesTotal = new int[constraintsToCreate];
        int[] constraintTuplesAllowed = new int[constraintsToCreate];

        for (int i = 0; i < constraintsToCreate; i++) {
            constraintTuplesTotal[i] = (int) Math.pow(this.domainSize, constraintArities.get(i));
            constraintTuplesAllowed[i] = (int) ((1 - this.q) * constraintTuplesTotal[i]);
            totalAllowedTuples += constraintTuplesAllowed[i];
        }

        if (constraintTighnessDeviationRatio > 0) {

            // Creating variance between individual constraints so that some are more restrictive than others
            int totalTuplesMissing = 0;
            for (int i = 0; i < constraintsToCreate; i++) {
                int removedTuples = (int) (constraintTuplesAllowed[i] * constraintTighnessDeviationRatio);
                constraintTuplesAllowed[i] -= removedTuples;
                totalTuplesMissing += removedTuples;
            }

            List<Integer> rebalancedConstraintIds = new ArrayList<>();
            for (int i = 0; i < constraintsToCreate; i++) {
                rebalancedConstraintIds.add(i);
            }
            Collections.shuffle(rebalancedConstraintIds, rnd);

            int it = 0;
            while (true) {

                int c_index = rebalancedConstraintIds.get(it);

                int allowance = constraintTuplesTotal[c_index] - constraintTuplesAllowed[c_index];
                int tuplesToReallocate = rnd.nextInt(allowance);
                if (tuplesToReallocate > totalTuplesMissing) {
                    tuplesToReallocate = totalTuplesMissing;
                }
                constraintTuplesAllowed[c_index] += tuplesToReallocate;
                totalTuplesMissing -= tuplesToReallocate;

                if (totalTuplesMissing == 0) {
                    break;
                }

                it++;
                if (it > constraintsToCreate - 1) {
                    it = 0;
                }
            }
        }

        // intitialising constraints with randomised allowed tuples
        int d = 0;
        seed++;
        for (int i = 0; i < constrVarIds.length; i++) {
            C[d] = new Constraint(constrVarIds[i].length, domainSize);
            for (int j = 0; j < constrVarIds[i].length; j++) {
                C[d].varGlobalIDs[j] = constrVarIds[i][j];
            }
            int targetAllowedTuples = constraintTuplesAllowed[d];
            //System.out.println("Constraint ["+i+"] : Enabling " + targetAllowedTuples + " / " + constraintTuplesTotal[i] + " tuples");
            C[d].randomiseAllowedTuples(targetAllowedTuples, seed + d);
            d++;
        }
        this.totalConstraints = constraintsToCreate;
    }
}
