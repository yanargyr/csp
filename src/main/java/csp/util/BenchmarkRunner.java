package csp.util;

import csp.solvers.Solver;
import csp.solvers.SolverFactory;
import csp.data.ProblemClass;
import csp.enums.HeuristicStrategy;
import csp.enums.ProblemState;
import csp.data.SolverResult;
import csp.heuristics.HeuristicFactory;
import csp.problems.Problem;
import csp.problems.RandomProblem;

import java.util.ArrayList;
import java.util.List;

public class BenchmarkRunner {

    /**
     * Creates a random CSP problem and solves it
     *
     * @param vars:      Total number of problem variables
     * @param dom:       Uniform variable domain size
     * @param min_arity: The minimum arity of the problem constraints
     * @param max_arity: The maximum arity of the problem constraints
     * @param p:         Constraint density. 0 will create 0 constraints. 1 will create the max available constraints,
     *                   For a = fixed arity, this would be:
     *                   totalVars! / (a! * (totalVars - a)!)
     * @param q:         Constraint tightness [0-1]. Bigger means tighter (fewer allowed tuples)
     * @param timeLimit: Solver time out limit in seconds
     * @param heuristic: Heuristic id to use
     * @param seed:      Random seed
     */
    public static void runSingleProblem(
            int vars,
            int dom,
            int min_arity,
            int max_arity,
            double p,
            double q,
            int timeLimit,
            int heuristic,
            int seed) {


        Problem P = new RandomProblem(vars, min_arity, max_arity, p, q, dom, seed);
        Solver s = SolverFactory.getDefaultsolver(P);
        s.solve(heuristic, timeLimit);

        System.out.println("");

        switch (s.problemState) {
            case SOLVED:
                System.out.println("Solved {");
                break;
            case UNSAT:
                System.out.println("UnSat  {");
                break;
            case INCONSISTENT:
                System.out.println("Not AC {");
                break;
            case TIMEDOUT:
                System.out.println("TimeOut{");
                break;
        }
        System.out.println(String.valueOf(s.timeTotal) + "} sec |  Max depth = " + s.maxDepth + " | backtracks : {"
                + s.backtracks + "} nodes: {" + s.nodes + "}");
    }


    public static void runFullTestSet(ProblemClass c,
                                      int runs_q,
                                      int runs_q2,
                                      int timeLimit,
                                      int seed,
                                      int heuristic) {
        runFullTestSet(c.vars, c.domainSize, c.arity, c.arity, c.p, 1 - c.q, 1 - c.q, runs_q, runs_q2, timeLimit, seed, heuristic);
    }

    /**
     * Creates a bunch of random CSP problems and solves them
     *
     * @param vars:      Total number of problem variables
     * @param dom:       Uniform variable domain size
     * @param min_arity: The minimum arity of the problem constraints
     * @param max_arity: The maximum arity of the problem constraints
     * @param fixedP:    Constraint density. 0 will create 0 constraints. 1 will create the max available constraints
     * @param minq:      Starting(minimum) constraint tightness [0-1]. Bigger means tighter (fewer allowed tuples)
     * @param maxq:      Maximum (Maximum) constraint tightness [0-1]. Bigger means tighter (fewer allowed tuples)
     * @param runs_q:    Variations of Constraint tightness inside the [minq - maxq] range
     * @param runs_q2:   Number of tests per each step of constraint tightness. Will eventually create runs_q * runs_q2 problems
     * @param timeLimit: Solver time out limit in seconds
     * @param seed:      Heuristic id to use
     * @param heuristic: Random seed
     */
    public static void runFullTestSet(int vars,
                                      int dom,
                                      int min_arity,
                                      int max_arity,
                                      double fixedP,
                                      double minq,
                                      double maxq,
                                      int runs_q,
                                      int runs_q2,
                                      int timeLimit,
                                      int seed,
                                      int heuristic) {
        double step_q, first_q;

        double totalTime = 0;
        double totalBacktracks = 0;
        double totalNodes = 0;
        double totalMaxDepth = 0;

        int totalSolved = 0;
        int totalUnsat = 0;
        int totalInconsistent = 0;
        int totalTimeOuts = 0;

        if (runs_q == 1) {
            step_q = 0;
        } else {
            step_q = (maxq - minq) / (double) (runs_q - 1);
        }
        first_q = minq;

        System.out.println("Q step :" + step_q);

        int totalTests = runs_q * runs_q2; // Total tests to run

        /*
            Problem attributes/stats, [run], [totalVars, dom, p, q]
         */
        double[][] probls = new double[runs_q][4];

        List<SolverResult> outcomes = new ArrayList<>();

        int run = 0;

        double q = first_q;
        for (int j = 0; j < runs_q; j++) {

            System.out.println("----- (1 - q): -----" + (1 - q));
            for (int t = 0; t < runs_q2; t++) {
                System.out.println("\n Problem: " + run + "/" + (runs_q * runs_q2));
                Problem p = new RandomProblem(vars, min_arity, max_arity, fixedP, q, dom, seed + t);
                Solver s = SolverFactory.getDefaultsolver(p);
                SolverResult outcome = s.solve(heuristic, timeLimit);

                switch (s.problemState) {
                    case SOLVED:
                        totalSolved++;
                        break;
                    case UNSAT:
                        totalUnsat++;
                        break;
                    case INCONSISTENT:
                        totalInconsistent++;
                        break;
                    case TIMEDOUT:
                        totalTimeOuts++;
                        break;
                }

                outcomes.add(outcome);
                //System.out.println(outcome.printSolution());
                System.out.println(outcome.printResult());

                totalTime += s.timeTotal;
                totalMaxDepth += s.maxDepth;
                totalBacktracks += s.backtracks;
                totalNodes += s.nodes;

                run++;
            }

            probls[j][0] = (double) vars;
            probls[j][1] = dom;
            probls[j][2] = fixedP;
            probls[j][3] = q;

            q += step_q;
        }


        int qVariations = 0;
        for (int k = 0; k < totalTests; k++) {
            if (k % (runs_q2) == 0)//&& k!=runs_p*runs_q )
            {
                System.out.println("\n\ntotalVars: " + probls[qVariations][0] + ", dom: " + probls[qVariations][1] + ", p: "
                        + probls[qVariations][2] + ", 1-q: " + (1 - probls[qVariations][3]) + " arity: " + max_arity);
                qVariations++;
            }
            outcomes.get(k).printResult();
        }


        System.out.println("Heuristic used : " + HeuristicFactory.getHeuristicDescription(heuristic));
        System.out.println("-------------------------");
        System.out.println("Average Time: " + totalTime / totalTests);
        System.out.println("Average MaxDepth: " + totalMaxDepth / totalTests);
        System.out.println("Average nodes: " + totalNodes / totalTests);
        System.out.println("Average backtracks: " + totalBacktracks / totalTests);

        System.out.println("-------------------------");
        System.out.println("Solved: " + totalSolved + "(" + (double) ((100 * totalSolved) / totalTests) + "%)");
        System.out.println("UnSat: " + totalUnsat);
        System.out.println("Non AC: " + totalInconsistent);
        System.out.println("TimeOuts: " + totalTimeOuts);

    }

    public static void runSudokuEasy() {
        int[] reserved = new int[81];
        reserved[2] = 3;
        reserved[4] = 2;
        reserved[6] = 6;
        reserved[9] = 9;
        reserved[12] = 3;
        reserved[14] = 5;
        reserved[17] = 1;
        reserved[20] = 1;
        reserved[21] = 8;
        reserved[23] = 6;
        reserved[24] = 4;
        reserved[29] = 8;
        reserved[30] = 1;
        reserved[32] = 2;
        reserved[33] = 9;
        reserved[36] = 7;
        reserved[44] = 8;
        reserved[47] = 6;
        reserved[48] = 7;
        reserved[50] = 8;
        reserved[51] = 2;
        reserved[56] = 2;
        reserved[57] = 6;
        reserved[59] = 9;
        reserved[60] = 5;
        reserved[63] = 8;
        reserved[66] = 2;
        reserved[68] = 3;
        reserved[71] = 9;
        reserved[74] = 5;
        reserved[76] = 1;
        reserved[78] = 3;


        solveSudoku(reserved, 9);
    }

    public static void runSudokuMedium() {
        int[] reserved = new int[81];
        reserved[1] = 1;
        reserved[9] = 6;
        reserved[10] = 8;
        reserved[11] = 7;
        reserved[16] = 1;
        reserved[22] = 9;
        reserved[23] = 7;
        reserved[26] = 3;
        reserved[36] = 7;
        reserved[42] = 3;
        reserved[43] = 4;
        reserved[46] = 4;
        reserved[48] = 9;
        reserved[49] = 2;
        reserved[50] = 6;
        reserved[56] = 9;
        reserved[57] = 6;
        reserved[59] = 5;
        reserved[62] = 2;
        reserved[65] = 2;
        reserved[67] = 3;
        reserved[72] = 5;
        reserved[76] = 1;
        reserved[77] = 9;
        reserved[78] = 7;

        solveSudoku(reserved, 9);
    }


    public static void runSudokuVeryHard() {
        //Finnish mathematician Arto Inkala claimed to have created the "World's Hardest Sudoku".
        int[] reserved = new int[81];
        reserved[0] = 8;
        reserved[11] = 3;
        reserved[19] = 7;
        reserved[12] = 6;
        reserved[22] = 9;
        reserved[24] = 2;
        reserved[28] = 5;
        reserved[32] = 7;
        reserved[40] = 4;
        reserved[41] = 5;
        reserved[48] = 1;
        reserved[42] = 7;
        reserved[52] = 3;
        reserved[56] = 1;
        reserved[65] = 8;
        reserved[73] = 9;
        reserved[66] = 5;
        reserved[61] = 6;
        reserved[62] = 8;
        reserved[70] = 1;
        reserved[78] = 4;

        solveSudoku(reserved, 9);
    }

    /**
     * Solves any Sudoku problem
     *
     * @param grid: an int array of length 81 representing the initial state of the grid. Contains 0 for unassigned/blank cells, and != 0 for reserved cells
     */
    public static void solveSudoku(final int[] grid, int rowSize) {

        int BOARD_SIZE = rowSize;

        SudokuUtil.printSudokuProblem(grid, rowSize);

        Problem P = ProblemGenerator.createSudokuProblem(grid, BOARD_SIZE);
        Solver s = SolverFactory.getDefaultsolver(P);
        SolverResult result = s.solve(HeuristicStrategy.EWDEG_FWDEGDOM, 60);

        SudokuUtil.printSudokuSolution(result, BOARD_SIZE);


        System.out.println(result.printResult());
    }

    public static void solveNQueenProblem(final int BOARD_SIZE) {

        String WHITE = "░";
        String BLACK = "▓";


        Problem p = ProblemGenerator.createNQueenProblem(BOARD_SIZE);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, 60);

        if (s.problemState == ProblemState.SOLVED) {

            for (int i = 0; i < BOARD_SIZE; i++) {
                System.out.println();
                for (int j = 0; j < BOARD_SIZE; j++) {
                    if (result.assignments[i] == j) {
                        System.out.print("Q");
                    } else {
                        if (i % 2 == 0) {
                            System.out.print(j % 2 == 0 ? BLACK : WHITE);
                        } else {
                            System.out.print(j % 2 == 0 ? WHITE : BLACK);
                        }
                    }
                }
            }
        }

        System.out.println();
        System.out.println(result.printResult());
    }

}
