package csp.util;

public class Util {
    /**
     * checks if all elements in an row of a given 2D array row, are unique
     * @param array
     * @param rowIndex
     * @return
     */
    public static boolean allRowElementsAreUnique(int[][] array, int rowIndex) //
    {
        boolean ok = true;
        for (int i = 0; i < array[rowIndex].length; i++) {
            for (int j = 0; j < array[rowIndex].length; j++) {
                if (array[rowIndex][i] == array[rowIndex][j] && i != j) ok = false;
            }
        }
        return ok;
    }

    /**
     * Checks if a row in a given 2D array row, is unique, i.e there is not other row that contains the same exact elements
     * NOTE: Will only compare with previous rows, not the next ones (Performance)
     * @param array
     * @param rowIndex
     * @return
     */
    public static boolean isRowUniqueCheckBackwardsOnly(int[][] array, int rowIndex) {
        boolean ok = true;
        int rowSize = array[rowIndex].length;  // in the context of a constraint, this is arity
        for (int x = 0; x < rowIndex; x++) {
            int same = 0;
            if (array[x].length == rowSize) { // Compare only with rows of same size
                for (int i = 0; i < rowSize; i++) {
                    for (int j = 0; j < rowSize; j++) {
                        if (array[x][i] == array[rowIndex][j]) {
                            same++;
                        }
                    }
                }
            }
            if (same >= rowSize) {
                ok = false;
            }
        }
        return ok;
    }

    public static double calcLogFact(int n) {
        double x = 0;
        for (int i = 1; i <= n; i++) {
            double d = i;
            x += Math.log(d);
        }
        return (x);
    }
}
