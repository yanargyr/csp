package csp.util;

import csp.constraints.Constraint;
import csp.constraints.ConstraintGenerator;
import csp.problems.Problem;

import java.util.ArrayList;
import java.util.List;

public class ProblemGenerator {

    public static Problem createNQueenProblem(final int BOARD_SIZE) {
        List<Constraint> constraints = new ArrayList<>();

        /*
         Grid works like this:
         Vars: rows
         Values: column where the queen is located in each row
          */


        // Column Constraints. Cannot have 2 rows having the queen in the same column
        for (int i = 0; i < BOARD_SIZE - 1; i++) {
            for (int j = i + 1; j < BOARD_SIZE; j++) {
                //Constraint c = ConstraintGenerator.createBinaryConstraintRestrictPairOfValues(i,j,1,1, BOARD_SIZE);
                Constraint c = ConstraintGenerator.createConstraintAllDifferent(new int[]{i, j}, BOARD_SIZE);
                constraints.add(c);
            }
        }

        // Populating diagonals \
        for (int var1 = 0; var1 < BOARD_SIZE - 1; var1++) {
            for (int col = 0; col < BOARD_SIZE - 1; col++) {
                for (int var2 = var1 + 1; var2 < BOARD_SIZE; var2++) {
                    int col2 = col + (var2 - var1);
                    if (col2 < BOARD_SIZE) {
                        Constraint c = ConstraintGenerator.createBinaryConstraintRestrictPairOfValues(var1, var2, col, col2, BOARD_SIZE);
                        constraints.add(c);
                    }
                }
            }
        }

        // Populating diagonals /
        for (int var1 = 0; var1 < BOARD_SIZE - 1; var1++) {
            for (int col1 = BOARD_SIZE - 1; col1 > 0; col1--) {
                for (int var2 = var1 + 1; var2 < BOARD_SIZE; var2++) {
                    int col2 = col1 - (var2 - var1);
                    if (col2 >= 0) {
                        Constraint c = ConstraintGenerator.createBinaryConstraintRestrictPairOfValues(var1, var2, col1, col2, BOARD_SIZE);
                        constraints.add(c);
                    }
                }
            }
        }

        return new Problem(constraints, BOARD_SIZE, BOARD_SIZE);
    }

    public static Problem createSudokuProblem(final int[] grid, int rowSize) {

        List<Constraint> constraints = new ArrayList<>();
        int BOARD_SIZE = rowSize;//9;
        int SUBGRID_SIZE = (int) Math.sqrt(BOARD_SIZE);
        int SUBGRIDS = BOARD_SIZE / SUBGRID_SIZE;

        //create row rules
        for (int row = 0; row < BOARD_SIZE; row++) {
            // using permutation to break up the all diff constraints in binary constraints
            // to avoid creating 9^9 tuples, as this was very slow and pushed the heap size
            for (int i = 0; i < BOARD_SIZE - 1; i++) {
                for (int j = i + 1; j < BOARD_SIZE; j++) {
                    int[] rowVars = new int[]{BOARD_SIZE * row + i, BOARD_SIZE * row + j};
                    Constraint c = ConstraintGenerator.createConstraintAllDifferent(rowVars, BOARD_SIZE);
                    constraints.add(c);
                }
            }
        }

        //create column rules
        for (int col = 0; col < BOARD_SIZE; col++) {
            for (int i = 0; i < BOARD_SIZE - 1; i++) {
                for (int j = i + 1; j < BOARD_SIZE; j++) {
                    int[] rowVars = new int[]{BOARD_SIZE * i + col, BOARD_SIZE * j + col};
                    Constraint c = ConstraintGenerator.createConstraintAllDifferent(rowVars, BOARD_SIZE);
                    constraints.add(c);
                }
            }
        }

        //create sub grid rules
        for (int gridrow = 0; gridrow < SUBGRIDS; gridrow++) {
            for (int gridcol = 0; gridcol < SUBGRIDS; gridcol++) {
                int[] subgridVars = new int[SUBGRID_SIZE * SUBGRID_SIZE];

                int START_ROW = gridrow * SUBGRID_SIZE;
                int START_COL = gridcol * SUBGRID_SIZE;

                int a = 0;

                for (int row = START_ROW; row < START_ROW + SUBGRID_SIZE; row++) {
                    for (int col = START_COL; col < START_COL + SUBGRID_SIZE; col++) {
                        subgridVars[a] = BOARD_SIZE * row + col;
                        a++;
                    }
                }
                for (int i = 0; i < subgridVars.length - 1; i++) {
                    for (int j = i + 1; j < subgridVars.length; j++) {
                        int[] vars = new int[]{subgridVars[i], subgridVars[j]};
                        Constraint c = ConstraintGenerator.createConstraintAllDifferent(vars, BOARD_SIZE);
                        constraints.add(c);
                    }
                }
            }
        }


        // Reserve occupied tiles as unary constraints
        for (int i = 0; i < grid.length; i++) {
            int val = grid[i];
            if (val != 0) {
                Constraint c = ConstraintGenerator.createUnaryConstraint(i, BOARD_SIZE, new int[]{val - 1});
                constraints.add(c);
            }
        }

        return new Problem(constraints, (int) Math.pow(BOARD_SIZE, 2), BOARD_SIZE);
    }
}
