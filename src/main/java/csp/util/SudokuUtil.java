package csp.util;

import csp.data.SolverResult;

public class SudokuUtil {

    public static void printSudokuProblem(final int[] grid, final int boardSize) {

        int BOARD_SIZE = boardSize;
        int SUBGRID_SIZE = (int) Math.sqrt(BOARD_SIZE);

        System.out.println(" >>>>> PROBLEM <<<<<");
        for (int i = 0; i < BOARD_SIZE; i++) {
            if (i % SUBGRID_SIZE == 0)
                printLine(BOARD_SIZE, SUBGRID_SIZE);
            else
                System.out.println();
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (j % SUBGRID_SIZE == 0)
                    System.out.print("|");

                int val = grid[BOARD_SIZE * i + j];
                String valStr;

                //useful for 16x16 Sudoku
                switch (val) {
                    case 10:
                        valStr = "A";
                        break;
                    case 11:
                        valStr = "B";
                        break;
                    case 12:
                        valStr = "C";
                        break;
                    case 13:
                        valStr = "D";
                        break;
                    case 14:
                        valStr = "E";
                        break;
                    case 15:
                        valStr = "F";
                        break;
                    case 16: {
                        valStr = "G";
                        break;
                    }
                    default: {
                        valStr = String.valueOf(val);
                    }
                }

                if (val != 0)
                    System.out.print(" " + valStr + " ");
                else
                    System.out.print("   ");

                if (j == BOARD_SIZE - 1)
                    System.out.print("|");
            }
        }
        printLine(BOARD_SIZE, SUBGRID_SIZE);
    }

    public static void printSudokuSolution(final SolverResult result, final int boardSize) {

        int BOARD_SIZE = boardSize;
        int SUBGRID_SIZE = (int) Math.sqrt(BOARD_SIZE);

        System.out.println(" >>>>> SOLUTION <<<<<");
        for (int i = 0; i < BOARD_SIZE; i++) {

            if (i % SUBGRID_SIZE == 0) {
                printLine(BOARD_SIZE, SUBGRID_SIZE);
            } else
                System.out.println();
            for (int j = 0; j < BOARD_SIZE; j++) {
                if (j % SUBGRID_SIZE == 0) {
                    System.out.print("|");
                }

                int val = result.assignments[BOARD_SIZE * i + j] + 1;
                String valStr;

                //useful for 16x16 Sudoku
                switch (val) {
                    case 10:
                        valStr = "A";
                        break;
                    case 11:
                        valStr = "B";
                        break;
                    case 12:
                        valStr = "C";
                        break;
                    case 13:
                        valStr = "D";
                        break;
                    case 14:
                        valStr = "E";
                        break;
                    case 15:
                        valStr = "F";
                        break;
                    case 16: {
                        valStr = "G";
                        break;
                    }
                    default: {
                        valStr = String.valueOf(val);
                    }
                }

                System.out.print(" " + valStr + " ");

                if (j == BOARD_SIZE - 1)
                    System.out.print("|");
            }
        }
        printLine(BOARD_SIZE, SUBGRID_SIZE);
    }

    public static void printLine(int BOARD_SIZE, int SUBGRID_SIZE) {
        int width = 3 * BOARD_SIZE + SUBGRID_SIZE + 1;
        System.out.println("");
        //System.out.println("\n-------------------------------");
        for (int j = 0; j < width; j++)
            System.out.print("-");
        System.out.println();
    }

}
