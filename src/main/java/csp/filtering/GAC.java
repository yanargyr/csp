package csp.filtering;

import csp.solvers.Solver;
import csp.constraints.Constraint;
import csp.data.Arc;
import csp.problems.Problem;

import java.util.ArrayList;

/**
 * Generalized Arc Consistency algorithm
 */
public class GAC implements Filtering {

    Problem p;
    Solver s;
    ArrayList<Arc> arcsToVisit;
    int lastRunChecks;

    public GAC(Solver s) {
        this.s = s;
        this.p = s.p;
    }

    @Override
    public boolean filter() {
        lastRunChecks = 0;
        this.arcsToVisit = new ArrayList<>();

        if (s.depth == 0 || s.currentBranchVar < 0) {
            // If there are no assignments at all, check all arcs
            for (int i = 0; i < p.totalVars; i++) {
                for (int j = 0; j < p.totalConstraints; j++) {
                    Constraint c = p.C[j];
                    if (c.containsVar(i)) {
                        addArc(i, j);
                    }

                }
            }
        } else {
            // Else start by adding arcs for each constraint involving the last assigned var
            for (int j = 0; j < p.totalConstraints; j++) {
                Constraint c = p.C[j];
                if (c.containsVar(s.currentBranchVar)) {
                    for (int i = 0; i < c.arity; i++) {
                        int otherVarId = c.varGlobalIDs[i];
                        // Don't bother for variables that are already assigned
                        if (s.assignments[otherVarId] == -1) {
                            addArc(otherVarId, j);
                        }
                    }
                }
            }
        }

        boolean wipeOut = false;
        while (!arcsToVisit.isEmpty()) {
            Arc arc = arcsToVisit.remove(0);
            // Visiting arc. Will remove it from the todo list

            int varGlobalId = arc.varGlobalId;
            int constraintIndex = arc.constraintIndex;
            Constraint c = p.C[constraintIndex];
            boolean atLeastOneValueRemoved = false;

            for (int v = 0; v < p.domainSize; v++) {
                // No need checking supports for vars that are already supportStatus, or the last assigned value
                if (s.hasSupport(varGlobalId, v) && !(varGlobalId == s.currentBranchVar && v == s.currentBranchValue)) {
                    boolean hasSupports = revise(varGlobalId, constraintIndex, v);
                    lastRunChecks++;
                    if (!hasSupports) {
                        // this value has no support after last assignment. Will have to remove it from the domain

//                        System.out.println("\n -->removing.. x"+ varGlobalId + "=" + v +" due to: x"
//                                + p.supportStatus[varGlobalId][v][0] + " = " + p.supportStatus[varGlobalId][v][1]
//                                + " Constraint: " + constraintIndex);

                        if (s.currentBranchVar < 0) {
                            s.markAsInitiallyUnsupported(varGlobalId, v);
                        } else {
                            s.markAsUnsupported(varGlobalId, v, s.currentBranchVar, s.currentBranchValue);
                        }
                        atLeastOneValueRemoved = true;

                        // Will revisit all arcs that might have been affected by last domain removal
                        // this includes all constraints that involved that variable, and all other involved variables
                        // If a wipeout has already occurred, we can stop the filtering right here.
                        // TWEAK : But just to get some more accurate constraint wighting lets finish the current arcs,
                        // and check arcs linked to the last assigned value. But will stop adding arcs for all other vars
                        if (!wipeOut || varGlobalId == s.currentBranchVar || s.currentBranchVar == -2) {
                            addLinkedArcs(varGlobalId);
                        }
                    }
                }
            }

            // Worth checking if we just had a domain wipeout.
            if (atLeastOneValueRemoved) {
                boolean noValueHasSupports = true;

                for (int v = 0; v < p.domainSize; v++) {
                    if (c.countSupportsDynamic(varGlobalId, v, s) > 0) {
                        noValueHasSupports = false;
                        break;
                    }
                }

                if (noValueHasSupports) {
                    wipeOut = true;
                    s.constraintConflicts[constraintIndex]++;
                }
            }
        }

        return !wipeOut;
    }

    private void addLinkedArcs(final int varGlobalId) {
        for (int x = 0; x < p.totalConstraints; x++) {
            Constraint c2 = p.C[x];
            if (c2.containsVar(varGlobalId)) {
                for (int i = 0; i < c2.arity; i++) {
                    int otherVarId = c2.varGlobalIDs[i];
                    if (s.assignments[otherVarId] == -1 && otherVarId != varGlobalId) {
                        addArc(otherVarId, x);
                    }
                }
            }
        }
    }

    private boolean revise(final int varGlobalId, final int constraintIndex, final int value) {
        Constraint c = p.C[constraintIndex];
        int supports = c.countSupportsDynamic(varGlobalId, value, s);

        return supports > 0;
    }


    private void addArc(final int varGlobalId, final int constraintIndex) {
        boolean arcAlreadyAdded = false;
        for (Arc arc : arcsToVisit) {
            if (arc.varGlobalId == varGlobalId && arc.constraintIndex == constraintIndex) {
                arcAlreadyAdded = true;
                break;
            }
        }

        if (!arcAlreadyAdded) {
            arcsToVisit.add(new Arc(varGlobalId, constraintIndex));
        }
    }
}
