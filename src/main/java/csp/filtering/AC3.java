package csp.filtering;

import csp.solvers.Solver;
import csp.constraints.Constraint;
import csp.problems.Problem;

/**
 * An implementation of MAC, AC3 Look Ahead algorithm
 */
public class AC3 implements Filtering {
    /**
     * The 'toVisit' array will be used as a guide for
     * which variables should be checked from the AC3 revise function.
     * Initially, it will be all '1's meaning that all variables are going to be checked.
     * When we check a variable without any domain deletions, it will change to '0'
     * and will stay that way until another variable, connected with it with any constraint,
     * gets a value deleted
     * <p>
     * When we have all '0's, meaning that we have no value deletions anymore
     * from arc consistency check, the algorithm will stop looping.
     * <p>
     * Note that when we say "check" a variable, we actually check all constraints containing it
     */
    int[] toVisit;
    Problem p;
    Solver s;
    int lastRunChecks;

    public AC3(Solver s) {
        this.s = s;
        this.p = s.p;
    }

    @Override
    public boolean filter() {
        lastRunChecks = 0;
        int vars = p.totalVars;
        int totalConstraints = p.totalConstraints;


        toVisit = new int[vars];
        if (s.currentBranchVar < 0) {
            for (int i = 0; i < vars; i++) {
                toVisit[i] = 1;
            }
        } else {
            for (int i = 0; i < vars; i++) {
                toVisit[i] = 0;
            }
            toVisit[s.currentBranchVar] = 1;
        }

        // all vars have at least one free value to assign (dynamic domain is not empty)
        boolean allVarsHaveValues = true;

        while (!noMoreVarsToVisit() && allVarsHaveValues) {
            //selecting a variable to check
            for (int i = 0; i < toVisit.length; i++) {
                // selecting a variable to analyze
                if (toVisit[i] == 1) {
                    toVisit[i] = 0;
                    //searching all constraints that contain this variable
                    for (int c = 0; c < totalConstraints; c++) {
                        if (p.C[c].containsVar(i)) {
                            reviseConstraint(p, c);
                        }
                    }
                }
            }

            /*
             * Checking if we have a variable with empty domain.
             * If we do, we should stop MAC as we've found an inconsistency
             */
            for (int i = 0; i < p.totalVars && allVarsHaveValues; i++) {
                int countSupportedValues = 0;
                for (int j = 0; j < p.domainSize; j++) {
                    if (s.hasSupport(i, j)) {
                        countSupportedValues++;
                    }
                }

                if (countSupportedValues == 0) {
                    allVarsHaveValues = false;
                    // we just noticed a domain wipe out");
                }
            }
        }
        return (allVarsHaveValues);
    }

    public boolean reviseConstraint(final Problem p, final int constraintIndex) {

        Constraint Cn = p.C[constraintIndex];

        // will stay false unless we wipe out a value from a variables domain
        boolean somethingChanged = false;

        // getting any variable of the constraint
        for (int i = 0; i < Cn.varGlobalIDs.length; i++) {
            //System.out.println("starting with x"+Cn.varGlobalIDs[i] + " local ID : "+i);

            int varGlobalID = Cn.varGlobalIDs[i];

            //checking for every value of the selected variable
            for (int j = 0; j < p.domainSize; j++) {

                if (s.hasSupport(Cn.varGlobalIDs[i], j)) {
                    int supports = Cn.countSupportsDynamic(varGlobalID, j, s);
                    lastRunChecks++;
                    Cn.supports[i][j] = supports;

                    /*
                     * checking if the new supportStatus value was already deleted
                     * If not we will have to revise again
                     */
                    if (supports == 0 && s.hasSupport(varGlobalID, j)) {

//                        System.out.println("\n -->removing.. x"+ Cn.varGlobalIDs[i]+ "=" + j+" due to: x"
//                                + p.supportStatus[Cn.varGlobalIDs[i]][j][0] + " = " + p.supportStatus[Cn.varGlobalIDs[i]][j][1]
//                                + " Constraint: " + constraintIndex);

                        //marking the variable as 'NON available'
                        s.markAsUnsupported(varGlobalID, j, s.currentBranchVar, s.currentBranchValue);
                        // having to remember that we will have to re-revise
                        somethingChanged = true;


                        // Now, adding all affected (from the value deletion) constraints and variables to be revisited
                        markVariableToRevise(i);

                        // Will make a quick check if this has caused a domain wipeout. If yes, will get bonus wipeout points
                        boolean hasFreeValues = false;
                        for (int v = 0; v < p.domainSize; v++) {
                            if (s.hasSupport(varGlobalID, v)) {
                                hasFreeValues = true;
                                break;
                            }
                        }
                        if (!hasFreeValues) {
                            s.constraintConflicts[constraintIndex]++;
                        }
                    }
                }
            }
        }
        return somethingChanged;
    }

    /**
     * Filtering if there are no more vars left to visit
     *
     * @return
     */
    public boolean noMoreVarsToVisit() {
        for (int i = 0; i < toVisit.length; i++) {
            if (toVisit[i] == 1)
                return false;
        }
        return true;
    }

    public void markVariableToRevise(int varId) {
        for (int i = 0; i < p.C.length; i++) {
            // finding all the constraints containing the given variable
            if (p.C[i].containsVar(varId)) {
                // marking all other variables contained in the constraint
                for (int j = 0; j < p.C[i].arity; j++) {
                    toVisit[j] = 1;
                }
            }
        }
    }
}
