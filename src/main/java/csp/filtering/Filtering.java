package csp.filtering;

public interface Filtering {


    /**
     * Perform filtering and pruning techniques on assigned problem
     * @return true if there are at least one free value to each unassigned var
     *         false if there was a domain wipe out (Need to backtrack)
     *
     * Note: if problem is inconsistent in 1sth depth, this is a special case of inconsistency
     */
    boolean filter();
}
