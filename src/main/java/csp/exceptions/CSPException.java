package csp.exceptions;

public class CSPException extends RuntimeException {
    public CSPException(String message) {
        super(message);
    }
}
