package csp.enums;

public final class ValueSupports {
    public static final int SUPPORTED = -1;
    public static final int UNSUPPORTED_INITIALLY = -2;
}
