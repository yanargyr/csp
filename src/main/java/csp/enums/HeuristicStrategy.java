package csp.enums;

public class HeuristicStrategy {
    public static final int FWDEGDOM = 0;
    public static final int WDEG_FWDEGDOM = 1;
    public static final int JW_FWDEGDOM = 2;
    public static final int MOM_FWDEGDOM = 3;
    public static final int EWDEG_FWDEGDOM = 4;
    public static final int DLCS_FWDEGDOM = 5;
    public static final int JW = 7;
    public static final int FWDEGDOM_JW = 8;
    public static final int FWDEGDOM_DLCS = 9;
    public static final int JW_DLCS = 10;
}
