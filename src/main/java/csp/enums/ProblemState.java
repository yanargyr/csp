package csp.enums;

public enum ProblemState {
    SOLVED,
    PENDING,
    UNSAT,
    INCONSISTENT,
    TIMEDOUT
}
