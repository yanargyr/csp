package csp.data;

import java.util.ArrayList;
import java.util.List;

public class ProblemClass {
    public int vars;
    public int domainSize;
    public int arity;
    public double p;
    public double q;



    public ProblemClass (int vars, int domainSize, int arity, double p, double q) {
        this.vars = vars;
        this.domainSize = domainSize;
        this.arity = arity;
        this.p = p;
        this.q = q;
    }

    public void printClassDescription(){
        System.out.println(" < " + this.arity +", " + this.vars + ", " + this.domainSize
                + ", " + p + ", " + this.q + " >");
    }


    public static List<ProblemClass> getDemoClasses() {
        // New rebalanced classes, that work with constraintTighnessDeviationRatio= 0.2
        List<ProblemClass> classes = new ArrayList<>();
        classes.add(new ProblemClass(35, 6, 2,  0.842, 0.915));
        classes.add(new ProblemClass(30, 6, 3,  0.02, 0.55));
        classes.add(new ProblemClass(14, 5, 4,  0.05, 0.65));
        classes.add(new ProblemClass(20, 10, 3,  0.05, 0.476));

        return classes;
    }
}

/**
 Original classes, they have different behavior now, mostly because of RandomProblem.java constraintTighnessDeviationRatio

 Classes from papers
< 2, 35, 6, 0.842, 0.889 >    //Thesis #1, #10, SETN #1
< 3, 30, 6, 0.01847, 0.5 >    //Thesis #3, #8, SETN #2
< 4, 14, 5, 0.05, 0.63 >      //Thesis #4,   SETN #3
< 3, 20, 10, 0.05, 0.45 >     //Thesis #9,   SETN #4
< 2, 100, 12, 0.4, 0.236 >    //Thesis #7,    SETN #5
< 3, 300, 5, 0.00022446, 0.1>

 Other Classes from thesis
< 2, 35, 9, 0.299, 0.667 >    //Thesis #2
< 3, 50, 20, 0.003, 0.05 >    //Thesis #5
< 3, 50, 20, 0.003, 0.15 >    //Thesis #6
 */
