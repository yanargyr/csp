package csp.data;

import csp.enums.ProblemState;

public class SolverResult {

    public ProblemState problemStatus;
    public float timeSpent;
    public int maxDepth;
    public int backtracks;
    public int nodes;
    public int heuristicUsed;
    public int[] assignments;

    public int vars;
    public int domain;
    public int constraints;

    public String printResult() {

        String outcomeStr;
        switch (problemStatus) {
            case SOLVED:
                outcomeStr = "Solved";
                break;
            case UNSAT:
                outcomeStr = "UnSat";
                break;
            case INCONSISTENT:
                outcomeStr = "Not AC";
                break;
            case TIMEDOUT:
                outcomeStr = "TimeOut";
                break;
            default:
                outcomeStr = "Error";
        }

        return new String(outcomeStr + " {" + timeSpent + "} sec |  Max depth = " + maxDepth
                + " | backtracks : {" + backtracks + "} nodes: {" + nodes + "}");
    }

    public String printStateUserFriendly() {
        String outcomeStr;
        switch (problemStatus) {
            case SOLVED:
                outcomeStr = "At least One Solution Exists";
                break;
            case UNSAT:
                outcomeStr = "Problem has no solution";
                break;
            case INCONSISTENT:
                outcomeStr = "Problem was initially inconsistent";
                break;
            case TIMEDOUT:
                outcomeStr = "Problem timed out";
                break;
            default:
                outcomeStr = "Error";
        }
        return outcomeStr;
    }

    public String printSolution() {
        if (problemStatus == ProblemState.SOLVED) {
            StringBuilder sol = new StringBuilder();

            sol.append("\n\n Printing solution : \n\n {");
            for (int g = 0; g < vars; g++) {
                if (g > 0)
                    sol.append(", ");
                sol.append("x" + g + "=" + this.assignments[g]);
            }
            sol.append("}\n\n");
            sol.append("\n " + constraints + "\n");
            return sol.toString();
        } else return "No sulution";

    }


}
