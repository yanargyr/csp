package csp.data;

public class Arc {

    public int varGlobalId;
    public int constraintIndex;

    public Arc(final int varGlobalId, final int constaintIndex) {
        this.constraintIndex = constaintIndex;
        this.varGlobalId = varGlobalId;
    }

}
