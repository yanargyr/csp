package csp.constraints;

import csp.exceptions.CSPException;

import java.util.ArrayList;

import static java.lang.Math.pow;

public class ConstraintGenerator {


    ////////////////////////////////////////////
    // Unary Constraint with restricted domain
    ////////////////////////////////////////////

    /**
     * Create a new unary constraint with restricted domain
     *
     * @param varGlobalID   The global scope id of the variable
     * @param domainSize   The variable's domain size
     * @param allowedValues The values that should be allowed.
     * @return
     */
    public static Constraint createUnaryConstraint(final int varGlobalID, final int domainSize, final int[] allowedValues) {
        Constraint c = new Constraint(1, domainSize);
        c.varGlobalIDs[0] = varGlobalID;
        setupUnaryConstraintWithDomainRestriction(c, allowedValues);
        return c;
    }

    public static void setupUnaryConstraintWithDomainRestriction(final Constraint c, final int[] allowedValues) {
        if (c.arity != 1) {
            throw new CSPException("Cannot setup Unary Constraint with domain restriction, constraint arity was " + c.arity);
        }
        if (allowedValues.length > c.domainSize) {
            throw new CSPException("Cannot setup Unary Constraint, allowedValues were " + allowedValues.length + ", but domain size was " + c.domainSize);
        }
        for (int i = 0; i < c.domainSize; i++) {
            c.tuples[i] = 0;
        }
        for (int i = 0; i < allowedValues.length; i++) {
            int value = allowedValues[i];
            if (value < 0 || value > c.domainSize - 1) {
                throw new CSPException("Cannot setup Unary Constraint, allowedValue " + value + " not in [0," + (c.domainSize - 1) + "]");
            }
            c.tuples[value] = 1;
        }
    }
    ////////////////////////////////////////////
    // Creating binary constraints
    ////////////////////////////////////////////


    /**
     * Creates a binary constraint of type !(x0=a ^ x1=b)
     *
     * @param var1GlobalID The global scope id of x0
     * @param var2GlobalID The global scope id of x1
     * @param var1value    The value id for 'a'
     * @param var2value    The value id for 'b'
     * @param domainSize   The variables domain size
     * @return
     */
    public static Constraint createBinaryConstraintRestrictPairOfValues(final int var1GlobalID, final int var2GlobalID, final int var1value, final int var2value, final int domainSize) {
        Constraint c = new Constraint(2, domainSize);
        c.varGlobalIDs[0] = var1GlobalID;
        c.varGlobalIDs[1] = var2GlobalID;

        int tupleSize = (int) pow(c.domainSize, c.arity);
        for (int i = 0; i < tupleSize; i++) {
            int value1 = c.getVarValueInTuple(c.getLocalIdForVariable(var1GlobalID), i);
            int value2 = c.getVarValueInTuple(c.getLocalIdForVariable(var2GlobalID), i);
            if (value1 == var1value && value2 == var2value) {
                c.tuples[i] = 0;
            } else {
                c.tuples[i] = 1;
            }
        }
        return c;
    }

    /**
     * Creates a binary constraint of type x0 < x1
     *
     * @param var1GlobalID The global scope id of x0
     * @param var2GlobalID The global scope id of x1
     * @param domainSize   The variables domainSize size
     * @return
     */
    public static Constraint createBinaryConstraintGreaterThat(final int var1GlobalID, final int var2GlobalID, final int domainSize) {
        Constraint c = new Constraint(2, domainSize);
        c.varGlobalIDs[0] = var1GlobalID;
        c.varGlobalIDs[1] = var2GlobalID;
        setupBinaryConstraintGreaterThan(c, var1GlobalID, var2GlobalID, false);
        return c;
    }

    /**
     * Creates a binary constraint of type x0 <= x1
     *
     * @param var1GlobalID The global scope id of x0
     * @param var2GlobalID The global scope id of x1
     * @param domainSize   The variables domainSize size
     * @return
     */
    public static Constraint createBinaryConstraintGreaterThatOrEquals(final int var1GlobalID, final int var2GlobalID, final int domainSize) {
        Constraint c = new Constraint(2, domainSize);
        c.varGlobalIDs[0] = var1GlobalID;
        c.varGlobalIDs[1] = var2GlobalID;
        setupBinaryConstraintGreaterThan(c, var1GlobalID, var2GlobalID, true);
        return c;
    }

    public static void setupBinaryConstraintGreaterThan(final Constraint c, final int var1GlobalID, final int var2GlobalID, final boolean allowEquals) {
        if (c.arity != 2) {
            throw new CSPException("Cannot setup binary constraint, constraint arity was " + c.arity);
        }

        int tupleSize = (int) pow(c.domainSize, c.arity);
        for (int i = 0; i < tupleSize; i++) {
            int value1 = c.getVarValueInTuple(c.getLocalIdForVariable(var1GlobalID), i);
            int value2 = c.getVarValueInTuple(c.getLocalIdForVariable(var2GlobalID), i);
            if (value1 > value2 || (allowEquals && value1 == value2)) {
                c.tuples[i] = 1;
            } else {
                c.tuples[i] = 0;
            }
        }
    }

    ////////////////////////////////////////////
    // Creating constraints of arity 2 or more
    ////////////////////////////////////////////

    /**
     * Creates a new constraint with 2 or more variables, where their values are always different
     *
     * @param varGlobalIDs The global scope ids of hte constraint variables
     * @param domainSize   The variables (uniform) domain size
     * @return
     */
    public static Constraint createConstraintAllDifferent(final int[] varGlobalIDs, final int domainSize) {
        int arity = varGlobalIDs.length;
        Constraint c = new Constraint(arity, domainSize);
        for (int i = 0; i < arity; i++) {
            c.varGlobalIDs[i] = varGlobalIDs[i];
        }
        setupConstraintAllDiff(c);
        return c;
    }

    public static void setupConstraintAllDiff(final Constraint c) {
        if (c.arity < 2) {
            throw new CSPException("Cannot create ALL DIFF constraint with " + c.arity + " totalVars, need 2 or more");
        }
        if (c.arity > c.domainSize) {
            throw new CSPException("Cannot create ALL DIFF constraint with " + c.arity + " totalVars, and domain size " + c.domainSize + ", need domaine size >= arity");
        }
        int tupleSize = (int) pow(c.domainSize, c.arity);
        for (int i = 0; i < tupleSize; i++) {
            int[] varValues = new int[c.arity];
            for (int j = 0; j < c.arity; j++) {
                varValues[j] = c.getVarValueInTuple(j, i);
            }
            ArrayList uniqueValues = new ArrayList();
            boolean allUnique = true;
            for (int j = 0; j < c.arity; j++) {
                if (uniqueValues.indexOf(varValues[j]) == -1) {
                    uniqueValues.add(varValues[j]);
                } else {
                    allUnique = false;
                    break;
                }
            }
            c.tuples[i] = allUnique ? 1 : 0;
        }

    }

    /**
     * Creates a new constraint with 2 or more variables, where their values are always equal
     *
     * @param varGlobalIDs The global scope ids of hte constraint variables
     * @param domainSize   The variables (uniform) domain size
     * @return
     */
    public static Constraint createConstraintAllEqual(final int[] varGlobalIDs, final int domainSize) {
        int arity = varGlobalIDs.length;
        Constraint c = new Constraint(arity, domainSize);
        for (int i = 0; i < arity; i++) {
            c.varGlobalIDs[i] = varGlobalIDs[i];
        }
        setupConstraintAllEqual(c);
        return c;
    }

    public static void setupConstraintAllEqual(final Constraint c) {
        if (c.arity < 2) {
            throw new CSPException("Cannot create ALL EQUAL constraint with " + c.arity + " vars, need 2 or more");
        }

        int tupleSize = (int) pow(c.domainSize, c.arity);
        for (int i = 0; i < tupleSize; i++) {
            int[] varValues = new int[c.arity];
            int firstValue = c.getVarValueInTuple(0, i);
            boolean allEqual = true;
            for (int j = 1; j < c.arity; j++) {
                if (c.getVarValueInTuple(j, i) != firstValue) {
                    allEqual = false;
                    break;
                }
            }
            c.tuples[i] = allEqual ? 1 : 0;
        }
    }

}
