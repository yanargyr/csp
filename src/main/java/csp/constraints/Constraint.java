package csp.constraints;/*
 * csp.Constraints.Constraint.java
 *
 * Created on December 8, 2007, 10:31 PM
 */

/*
 * About tuples[] matrix :
 * This will hold the allowed tuples of a constraint. instead of using a 2 dimension [n][n]  matrix, I use a
 * 1 dimension [dom^n] matrix. It will hold '0' for not allowed, '1' for allowed variable assignment. Examples:
 *
 *  x0=0, x1=0,......xn=0:
 *     0     0 ......   0   -> 0*domx^n-1 + 0*dom^n-2 +.....+ 0*dom^0 = 0.
 *                              tuples[0] = 0 means that the assignment {x0=0, x1=0,......xn=0} is conflicting
 *
 *  x0=0, x1=0,......xn=1:
 *     0     0 ......   1   -> 0*dom^n-1 + 0*dom^n-2 +.....+ 1*dom^0 = 1.
 *                          e.g : tuples[1] = 1 means that the assignment {x0=0, x1=0,......xn=1} is allowed
 *  .
 *  .
 *  .
 *  .
 *  x0=1, x1=1,......xn=1:
 *     1     1 ......   1   -> 1*dom^n-1 + 1*dom^n-2 +.....+ 1*dom^0 = ...dom^(n-1)
 *
 ***********************************************
 *
 * Example 2 :
 * For a constraint between 4 variables (n=4) with domain size = 3 (dom=3):
 *
 *  x0=0, x1=2, x2=1, x3=2:
 *     0     2`    1     2   -> 0*3^3 + 2*3^2 + 1*3^1 + 2*3^0 = 0 + 18 + 3 + 2 =  23
 *                           tuples[23] = 1 means that the assignment {x0 =0, x1= 2, x2=1, x3=2} is allowed
 *
 * note that:
 * 1) the matrix index corresponds to a unique variables assignment
 * 2) the variable index (the 'i' in xi) corresponds to the 'i-th' variable involved in the constraint,
 * not the global xi variable. To track back to the global scope id of the variable, look up into varGlobalIDs[] array
 *
 *
 */

import csp.solvers.Solver;
import csp.exceptions.CSPException;

import java.util.*;

import static csp.enums.ValueSupports.SUPPORTED;
import static java.lang.Math.pow;

/**
 * @author admin
 */
public class Constraint {

    public int domainSize;
    public int arity;
    public int satisfied;
    /**
     * The global scope ids of each var in that constraint
     * Example:
     * if this constraint contains 3 vars
     * and varGlobalIDs = [0,9,12]
     * then the totalVars in this constraint are x0, x9 and x12
     */
    public int varGlobalIDs[];
    public int[] tuples;
    public int[][] supports;

    Random rnd2;

    public boolean outputting = false;

    /**
     * Instantiates a new instance of csp.Constraints.Constraint
     * Just instantiates the arrays, does not set allowed tuples
     * neither sets variable global ids
     */
    public Constraint(int arity, int domainSize) {
        this.domainSize = domainSize;
        this.satisfied = 0;
        this.arity = arity;
        this.varGlobalIDs = new int[arity];
        this.supports = new int[arity][domainSize];
        this.tuples = new int[(int) pow(domainSize, arity)];

        for (int i = 0; i < arity; i++) {
            for (int j = 0; j < domainSize; j++) {
                this.supports[i][j] = 0;
            }
        }
    }

    public void setConstraintTuples(int[] newTuples) {
        this.tuples = Arrays.copyOf(newTuples, newTuples.length);
    }

    public void setVarIds(int[] varGlobalIDs) {
        this.varGlobalIDs = Arrays.copyOf(varGlobalIDs, varGlobalIDs.length);
    }

    /**
     * Randomizes the allowed tuples of the constraint
     *
     * @param allowedTuplesToCreate how many tuples should be allowed
     * @param seed                  seed for the random algorithm
     */
    public void randomiseAllowedTuples(int allowedTuplesToCreate, int seed) {
        if (allowedTuplesToCreate > tuples.length) {
            throw new CSPException("Cannot create " + allowedTuplesToCreate + " tuples in constraint. max is " + tuples.length);
        }

        int allowedTuples = 0;
        this.rnd2 = new Random(seed);

        for (int i = 0; i < tuples.length; i++) {
            tuples[i] = 0;
        }

        // Will mark exactly 'targetAllowedTuples' tuples as allowed.
        List<Integer> tuplePriority = new ArrayList<>();
        for (int i = 0; i < tuples.length; i++) {
            tuplePriority.add(i);
        }
        Collections.shuffle(tuplePriority, rnd2);

        for (int i = 0; i < allowedTuplesToCreate; i++) {
            int tuple_id = tuplePriority.get(i);
            tuples[tuple_id] = 1;
            allowedTuples++;
        }

        if (outputting) {
            for (int i = 0; i < tuples.length; i++) {
                System.out.print("\n");
                for (int k = 0; k < arity; k++) {
                    System.out.print(getVarValueInTuple(k, i) + ",");
                }
                System.out.print(" : " + tuples[i]);
            }
            System.out.print("\n total tuples: " + tuples.length);
            System.out.print("\n generated " + allowedTuples + " tuples for ");
            printConstrVarIds();
        }
    }

    /**
     * Finds the value of a var in a specific tuple
     *
     * @param varLocalIndex local/constraint scope id for var
     * @param tupleIndex
     * @return
     */
    public int getVarValueInTuple(int varLocalIndex, int tupleIndex) {
        int res = 0;
        int temp = tupleIndex;
        for (int i = arity; i > varLocalIndex; i--) {
            int z = domainSize;
            res = temp % z;
            temp = temp / z;
        }
        // No need to actually access the tuples array
        return (res);
    }

    @Deprecated
    /**
     * Counts all the allowed tuples containing this this var / value (static support)
     *
     */
    public int countSupports(int var, int value) {
        int supports = 0;
        for (int i = 0; i < tuples.length; i++) {
            if (getVarValueInTuple(var, i) == value) {
                supports += tuples[i];
            }
        }
        return (supports);
    }

    /**
     * Counts dynamic supports for the assignment of a specific value to a specific variable
     *
     * @param varGlobalId
     * @param value
     * @param P
     * @return
     */
    public int countSupportsDynamic(int varGlobalId, int value, Solver s) {

        int var = getLocalIdForVariable(varGlobalId);
        int supports = 0;

        /*
         Will iterate all tuples where var = value
         For each of these tuples, we'll be checking against the other var/values in this tuple. If any of them were wiped off,
         or assigned with another value, there is no support

         No need to iterate all tuples, for example, in a constraint with arity = 3 and domainSize = 2:
            tuples[0] = 0 | (x0=0, x1=0, x2=0) is NOT allowed
            tuples[1] = 0 | (x0=0, x1=0, x2=1) is NOT allowed
            tuples[2] = 1 | (x0=0, x1=1, x2=0) is allowed
            tuples[3] = 0 | (x0=0, x1=1, x2=1) is NOT allowed
            tuples[4] = 1 | (x0=1, x1=0, x2=0) is allowed
            tuples[5] = 1 | (x0=1, x1=0, x2=1) is allowed
            tuples[6] = 1 | (x0=1, x1=1, x2=0) is allowed
            tuples[7] = 0 | (x0=1, x1=1, x2=1) is NOT allowed

            if we were looking for supports of var x1 with value = 1, we just need to check tuples 2,3,6,7
          */


        int rows = (int) pow(domainSize, (arity - var - 1));
        for (int a = 0; a < pow(domainSize, arity) / (rows * domainSize); a++) {
            for (int b = 0; b < rows; b++) {
                int g = a * domainSize * rows + rows * value + b;

                if (tuples[g] == 1) {

                    // we will see now whether the other variable/domains
                    // in that tuple are STILL available and not wiped off
                    int unsupported = 0;   //will stay "1" if we don't find a wiped out value
                    for (int j = 0; j < arity; j++) {
                        int k = getVarValueInTuple(j, g);

                        // will check if x_j = k was wiped out
                        // or and x_j is already assigned with another value
                        if (!s.hasSupport(varGlobalIDs[j], k) || (s.assignments[varGlobalIDs[j]] != SUPPORTED && s.assignments[varGlobalIDs[j]] != k))
                            unsupported++;
                    }
                    if (unsupported == 0) {
                        supports++;
                    }
                }
            }
        }
        if (outputting) {
            if (supports == 0) System.out.print("\n x" + varGlobalIDs[var] + " = " + value + " has NO support");
            else System.out.print("\n x" + varGlobalIDs[var] + " = " + value + " has " + supports + " support");
        }
        return (supports);
    }


    /**
     * Returns if this constraint is satisfied with a given assignment of values
     * if there is at least one tuple that supports this assignment, then yes
     *
     * @param assignments
     * @return
     */
    public boolean isSatisfied(int[] assignments) {
        boolean sat = false;
        int[] sup = new int[this.tuples.length];
        for (int i = 0; i < this.tuples.length; i++) {
            sup[i] = tuples[i];
        }

        for (int i = 0; i < arity; i++) {
            int globalVarId = this.varGlobalIDs[i];
            if (assignments[globalVarId] != -1) {
                for (int j = 0; j < tuples.length; j++) {
                    if (this.getVarValueInTuple(i, j) != assignments[globalVarId]) {
                        // TODO: it might already be 0 from another var in this tuple, or tuple initially being not allowed, this could be optimised further
                        sup[j] = 0;
                    }
                }
            }
        }

        for (int i = 0; i < this.tuples.length; i++) {
            //there is at least one tuple that supports this assignment, so yes
            if (sup[i] == 1) sat = true;
        }

        return sat;
    }

    /**
     * Returns whether the constraint contains a given variable or not
     *
     * @param varGlobalId
     * @return
     */
    public boolean containsVar(int varGlobalId) {
        for (int i = 0; i < varGlobalIDs.length; i++) {
            if (varGlobalIDs[i] == varGlobalId) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the local (constraint) scope id given a var's global id
     *
     * @param varGlobalId
     * @return
     */
    public int getLocalIdForVariable(int varGlobalId) {
        int localId = -1;
        for (int i = 0; i < varGlobalIDs.length; i++)
            if (varGlobalIDs[i] == varGlobalId) {
                localId = i;
            }

        if (localId == -1) {
            throw new CSPException("Constraint does not contain var with global id = " + varGlobalId);
        }
        return localId;
    }

    /*
    Prints global id for the variables in this constraint
     */
    public void printConstrVarIds() {
        System.out.print(" {");
        for (int i = 0; i < arity; i++) {
            if (i > 0)
                System.out.print(", ");
            System.out.print("x" + varGlobalIDs[i]);
        }
        System.out.print("} ");
    }

    public void printSupports(Solver s) {
        for (int k = 0; k < arity; k++) {
            for (int m = 0; m < domainSize; m++) {
                System.out.print("\n supports for x" + k + " = " + m + " " + countSupportsDynamic(varGlobalIDs[k], m, s));
            }
        }
    }

    public void printAllowed() {
        System.out.print("\n Printing Constraint : {");
        for (int i = 0; i < arity; i++) {
            System.out.print("x" + varGlobalIDs[i] + ",");
        }
        System.out.print("}\n ");

        for (int i = 0; i < (int) pow(domainSize, arity); i++) {
            for (int j = 0; j < arity; j++) {
                System.out.print(getVarValueInTuple(j, i) + ",");
            }
            System.out.print("| " + tuples[i] + "... " + i + " \n ");
        }

        for (int k = 0; k < arity; k++)
            for (int m = 0; m < domainSize; m++)
                System.out.print("\n supports for x" + k + " = " + m + " " + countSupports(k, m));

    }

}
