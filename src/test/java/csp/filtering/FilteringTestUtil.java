package csp.filtering;

import csp.constraints.Constraint;
import csp.constraints.ConstraintGenerator;
import csp.problems.Problem;

import java.util.ArrayList;
import java.util.List;

public class FilteringTestUtil {

    // a consistent problem
    public static Problem createPloblem1(){

        Constraint c0 = new Constraint(3, 2);
        c0.tuples[0] = 0;
        c0.tuples[1] = 0;
        c0.tuples[2] = 0;
        c0.tuples[3] = 1;
        c0.tuples[4] = 0;
        c0.tuples[5] = 1;
        c0.tuples[6] = 0;
        c0.tuples[7] = 0;

        c0.varGlobalIDs[0] = 0;
        c0.varGlobalIDs[1] = 1;
        c0.varGlobalIDs[2] = 2;

        Constraint c1 = new Constraint(3, 2);
        c1.tuples[0] = 1;
        c1.tuples[1] = 0;
        c1.tuples[2] = 1;
        c1.tuples[3] = 0;
        c1.tuples[4] = 0;
        c1.tuples[5] = 1;
        c1.tuples[6] = 0;
        c1.tuples[7] = 0;

        c1.varGlobalIDs[0] = 0;
        c1.varGlobalIDs[1] = 1;
        c1.varGlobalIDs[2] = 2;

        Constraint c2 = new Constraint(3, 2);
        c2.tuples[0] = 0;
        c2.tuples[1] = 1;
        c2.tuples[2] = 1;
        c2.tuples[3] = 0;
        c2.tuples[4] = 1;
        c2.tuples[5] = 1;
        c2.tuples[6] = 1;
        c2.tuples[7] = 0;

        c2.varGlobalIDs[0] = 2;
        c2.varGlobalIDs[1] = 1;
        c2.varGlobalIDs[2] = 3;

        Constraint c3 = new Constraint(3, 2);
        c3.tuples[0] = 1;
        c3.tuples[1] = 1;
        c3.tuples[2] = 1;
        c3.tuples[3] = 0;
        c3.tuples[4] = 0;
        c3.tuples[5] = 1;
        c3.tuples[6] = 1;
        c3.tuples[7] = 1;

        c3.varGlobalIDs[0] = 0;
        c3.varGlobalIDs[1] = 2;
        c3.varGlobalIDs[2] = 3;

        List<Constraint> constraints = new ArrayList<>();
        constraints.add(c0);
        constraints.add(c1);
        constraints.add(c2);
        constraints.add(c3);

        Problem p = new Problem(constraints, 4, 2);

        return p;
    }

    // a tighter problem. Will be inconsistent if x3 = 1
    public static Problem createPloblem2(){

        Constraint c0 = new Constraint(3, 2);
        c0.tuples[0] = 0;
        c0.tuples[1] = 1;
        c0.tuples[2] = 0;
        c0.tuples[3] = 0;
        c0.tuples[4] = 1;
        c0.tuples[5] = 1;
        c0.tuples[6] = 1;
        c0.tuples[7] = 0;

        c0.varGlobalIDs[0] = 0;
        c0.varGlobalIDs[1] = 1;
        c0.varGlobalIDs[2] = 2;


        Constraint c1 = new Constraint(3, 2);
        c1.tuples[0] = 0;
        c1.tuples[1] = 1;
        c1.tuples[2] = 1;
        c1.tuples[3] = 0;
        c1.tuples[4] = 0;
        c1.tuples[5] = 1;
        c1.tuples[6] = 0;
        c1.tuples[7] = 0;

        c1.varGlobalIDs[0] = 1;
        c1.varGlobalIDs[1] = 2;
        c1.varGlobalIDs[2] = 3;

        Constraint c2 = new Constraint(3, 2);
        c2.tuples[0] = 1;
        c2.tuples[1] = 0;
        c2.tuples[2] = 1;
        c2.tuples[3] = 0;
        c2.tuples[4] = 1;
        c2.tuples[5] = 1;
        c2.tuples[6] = 0;
        c2.tuples[7] = 1;

        c2.varGlobalIDs[0] = 2;
        c2.varGlobalIDs[1] = 3;
        c2.varGlobalIDs[2] = 0;

        Constraint c3 = new Constraint(3, 2);
        c3.tuples[0] = 1;
        c3.tuples[1] = 1;
        c3.tuples[2] = 1;
        c3.tuples[3] = 1;
        c3.tuples[4] = 1;
        c3.tuples[5] = 1;
        c3.tuples[6] = 0;
        c3.tuples[7] = 0;

        c3.varGlobalIDs[0] = 3;
        c3.varGlobalIDs[1] = 0;
        c3.varGlobalIDs[2] = 1;

        List<Constraint> constraints = new ArrayList<>();
        constraints.add(c0);
        constraints.add(c1);
        constraints.add(c2);
        constraints.add(c3);

        Problem p = new Problem(constraints, 4, 2);


        return p;
    }

    // an initially inconsitent problem.
    public static Problem createPloblem3(){

        Constraint c0 = new Constraint(3, 2);
        c0.tuples[0] = 0;
        c0.tuples[1] = 1;
        c0.tuples[2] = 0;
        c0.tuples[3] = 0;
        c0.tuples[4] = 1;
        c0.tuples[5] = 1;
        c0.tuples[6] = 1;
        c0.tuples[7] = 0;

        c0.varGlobalIDs[0] = 0;
        c0.varGlobalIDs[1] = 1;
        c0.varGlobalIDs[2] = 2;


        Constraint c1 = new Constraint(3, 2);
        c1.tuples[0] = 0;
        c1.tuples[1] = 1;
        c1.tuples[2] = 0;
        c1.tuples[3] = 0;
        c1.tuples[4] = 0;
        c1.tuples[5] = 1;
        c1.tuples[6] = 0;
        c1.tuples[7] = 0;

        c1.varGlobalIDs[0] = 1;
        c1.varGlobalIDs[1] = 2;
        c1.varGlobalIDs[2] = 3;

        Constraint c2 = new Constraint(3, 2);
        c2.tuples[0] = 0;
        c2.tuples[1] = 1;
        c2.tuples[2] = 0;
        c2.tuples[3] = 0;
        c2.tuples[4] = 0;
        c2.tuples[5] = 1;
        c2.tuples[6] = 0;
        c2.tuples[7] = 1;

        c2.varGlobalIDs[0] = 2;
        c2.varGlobalIDs[1] = 3;
        c2.varGlobalIDs[2] = 0;

        Constraint c3 = new Constraint(3, 2);
        c3.tuples[0] = 1;
        c3.tuples[1] = 1;
        c3.tuples[2] = 1;
        c3.tuples[3] = 0;
        c3.tuples[4] = 1;
        c3.tuples[5] = 1;
        c3.tuples[6] = 0;
        c3.tuples[7] = 0;

        c3.varGlobalIDs[0] = 3;
        c3.varGlobalIDs[1] = 0;
        c3.varGlobalIDs[2] = 1;

        List<Constraint> constraints = new ArrayList<>();
        constraints.add(c0);
        constraints.add(c1);
        constraints.add(c2);
        constraints.add(c3);

        Problem p = new Problem(constraints, 4, 2);

        return p;
    }

    public static Problem createSudokuProblem() {
        //Finnish mathematician Arto Inkala claimed to have created the "World's Hardest Sudoku".
        int[] reserved = new int[81];
        reserved[0] = 8;
        reserved[11] = 3;
        reserved[19] = 7;
        reserved[12] = 6;
        reserved[22] = 9;
        reserved[24] = 2;
        reserved[28] = 5;
        reserved[32] = 7;
        reserved[40] = 4;
        reserved[41] = 5;
        reserved[48] = 1;
        reserved[42] = 7;
        reserved[52] = 3;
        reserved[56] = 1;
        reserved[65] = 8;
        reserved[73] = 9;
        reserved[66] = 5;
        reserved[61] = 6;
        reserved[62] = 8;
        reserved[70] = 1;
        reserved[78] = 4;

        List<Constraint> constraints = new ArrayList<>();
        int BOARD_SIZE = 9;
        int SUBGRID_SIZE = 3;
        int SUBGRIDS = BOARD_SIZE / SUBGRID_SIZE;

        //create row rules
        for (int row = 0; row < BOARD_SIZE; row++) {
            // using permutation to break up the all diff constraints in binary constraints
            // to avoid creating 9^9 tuples, as this was very slow and pushed the heap size
            for (int i = 0; i < BOARD_SIZE - 1; i++) {
                for (int j = i + 1; j < BOARD_SIZE; j++) {
                    int[] rowVars = new int[]{BOARD_SIZE * row + i, BOARD_SIZE * row + j};
                    Constraint c = ConstraintGenerator.createConstraintAllDifferent(rowVars, BOARD_SIZE);
                    constraints.add(c);
                }
            }
        }

        //create column rules
        for (int col = 0; col < BOARD_SIZE; col++) {
            for (int i = 0; i < BOARD_SIZE - 1; i++) {
                for (int j = i + 1; j < BOARD_SIZE; j++) {
                    int[] rowVars = new int[]{BOARD_SIZE * i + col, BOARD_SIZE * j + col};
                    Constraint c = ConstraintGenerator.createConstraintAllDifferent(rowVars, BOARD_SIZE);
                    constraints.add(c);
                }
            }
        }

        //create sub grid rules
        for (int gridrow = 0; gridrow < SUBGRIDS; gridrow++) {
            for (int gridcol = 0; gridcol < SUBGRIDS; gridcol++) {
                int[] subgridVars = new int[SUBGRID_SIZE * SUBGRID_SIZE];

                int START_ROW = gridrow * SUBGRID_SIZE;
                int START_COL = gridcol * SUBGRID_SIZE;

                int a = 0;

                for (int row = START_ROW; row < START_ROW + SUBGRID_SIZE; row++) {
                    for (int col = START_COL; col < START_COL + SUBGRID_SIZE; col++) {
                        subgridVars[a] = BOARD_SIZE * row + col;
                        a++;
                    }
                }
                for (int i = 0; i < subgridVars.length - 1; i++) {
                    for (int j = i + 1; j < subgridVars.length; j++) {
                        int[] vars = new int[]{subgridVars[i], subgridVars[j]};
                        Constraint c = ConstraintGenerator.createConstraintAllDifferent(vars, BOARD_SIZE);
                        constraints.add(c);
                    }
                }
            }
        }


        // Reserve occupied tiles as unary constraints
        for (int i = 0; i < reserved.length; i++) {
            int val = reserved[i];
            if (val != 0) {
                Constraint c = ConstraintGenerator.createUnaryConstraint(i, 9, new int[]{val - 1});
                constraints.add(c);
            }
        }


        Problem p = new Problem(constraints, 81, 9);
        return p;
    }
}
