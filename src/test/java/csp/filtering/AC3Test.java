package csp.filtering;

import csp.solvers.Solver;
import csp.solvers.SolverFactory;
import csp.problems.Problem;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class AC3Test {


    @Test
    public void testFilterProblem1IsConsistentIfX2is1AndSomeValuesAreUnsupported() {
        Problem p = FilteringTestUtil.createPloblem1();
        Solver s = SolverFactory.getDefaultsolver(p);
        AC3 ac3 = new AC3(s);
        s.assign(2, 1);

        boolean isConsistent = ac3.filter();
        assertTrue(isConsistent);

        assertFalse(s.hasSupport(0, 0));
        assertTrue(s.hasSupport(0, 1));
        assertTrue(s.hasSupport(1, 0));
        assertFalse(s.hasSupport(1, 1));
        assertFalse(s.hasSupport(2, 0));
        assertTrue(s.hasSupport(2, 1));
        assertTrue(s.hasSupport(3, 0));
        assertTrue(s.hasSupport(3, 1));
    }

    @Test
    public void testFilterProblem1IsConsistentWithNoAssignments() {
        Problem p = FilteringTestUtil.createPloblem1();
        Solver s = SolverFactory.getDefaultsolver(p);
        AC3 ac3 = new AC3(s);

        boolean isConsistent = ac3.filter();
        assertTrue(isConsistent);

        assertFalse(s.hasSupport(0, 0));
        assertTrue(s.hasSupport(0, 1));
        assertTrue(s.hasSupport(1, 0));
        assertFalse(s.hasSupport(1, 1));
        assertFalse(s.hasSupport(2, 0));
        assertTrue(s.hasSupport(2, 1));
        assertTrue(s.hasSupport(3, 0));
        assertTrue(s.hasSupport(3, 1));
    }

    @Test
    public void testFilterProblem2IsInconsistentIfX3is1() {
        Problem p = FilteringTestUtil.createPloblem2();

        Solver s = SolverFactory.getDefaultsolver(p);
        AC3 ac3 = new AC3(s);
        s.assign(3, 1);
        s.depth = 1;

        boolean isConsistent = ac3.filter();
        assertFalse(isConsistent);
    }

    @Test
    public void testFilterProblem2IsConsistentIfX3is0() {
        Problem p = FilteringTestUtil.createPloblem2();
        Solver s = SolverFactory.getDefaultsolver(p);
        AC3 ac3 = new AC3(s);
        s.assign(3, 0);
        s.depth = 1;

        boolean isConsistent = ac3.filter();
        assertTrue(isConsistent);
    }


    @Test
    public void testFilterProblem2IsConsistentWithNoAssignmentsAndAllValuesAreSupported() {
        Problem p = FilteringTestUtil.createPloblem2();

        Solver s = SolverFactory.getDefaultsolver(p);
        AC3 ac3 = new AC3(s);

        boolean isConsistent = ac3.filter();
        assertTrue(isConsistent);

        assertTrue(s.hasSupport(0, 0));
        assertTrue(s.hasSupport(0, 1));
        assertTrue(s.hasSupport(1, 0));
        assertTrue(s.hasSupport(1, 1));
        assertTrue(s.hasSupport(2, 0));
        assertTrue(s.hasSupport(2, 1));
        assertTrue(s.hasSupport(3, 0));
        assertTrue(s.hasSupport(3, 1));
    }

    @Test
    public void testFilterProblem3IsInitiallyInconsistent() {
        Problem p = FilteringTestUtil.createPloblem3();

        Solver s = SolverFactory.getDefaultsolver(p);
        AC3 ac3 = new AC3(s);

        boolean isConsistent = ac3.filter();
        assertFalse(isConsistent);
    }

    @Test
    public void testFilterSudokuProblemIsConsistent() {
        Problem p = FilteringTestUtil.createSudokuProblem();

        Solver s = SolverFactory.getDefaultsolver(p);
        AC3 ac3 = new AC3(s);

        boolean isConsistent = ac3.filter();
        assertTrue(isConsistent);
    }
}
