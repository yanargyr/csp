package csp.solvers;

import csp.constraints.Constraint;
import csp.constraints.ConstraintGenerator;
import csp.data.SolverResult;
import csp.enums.HeuristicStrategy;
import csp.enums.ProblemState;
import csp.problems.Problem;
import csp.util.ProblemGenerator;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.pow;
import static org.junit.Assert.assertEquals;

public class MACSolverTest {

    @Test
    public void testProblem1HasSolution() {

        //Vars: 5
        //Domain : 4
        List<Constraint> cons = new ArrayList<>();

        // x0 != x2
        cons.add(ConstraintGenerator.createConstraintAllDifferent(new int[]{0, 2}, 4));
        // x1 > x2
        cons.add(ConstraintGenerator.createBinaryConstraintGreaterThat(1, 2, 4));

        // x4 > x3
        cons.add(ConstraintGenerator.createBinaryConstraintGreaterThat(4, 3, 4));

        // x0 = x1 = x3
        cons.add(ConstraintGenerator.createConstraintAllEqual(new int[]{0, 1, 3}, 4));

        // Domain restrictions
        cons.add(ConstraintGenerator.createUnaryConstraint(1, 4, new int[]{0, 1}));
        cons.add(ConstraintGenerator.createUnaryConstraint(4, 4, new int[]{2}));
        cons.add(ConstraintGenerator.createUnaryConstraint(3, 4, new int[]{1, 2, 3}));
        cons.add(ConstraintGenerator.createUnaryConstraint(0, 4, new int[]{0, 1, 3}));

        Problem p = new Problem(cons, 5, 4);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, 10);

        assertEquals(ProblemState.SOLVED, result.problemStatus);
        assertEquals(1, result.assignments[0]);
        assertEquals(1, result.assignments[1]);
        assertEquals(0, result.assignments[2]);
        assertEquals(1, result.assignments[0]);
        assertEquals(2, result.assignments[4]);
    }


    @Test
    public void testProblem2HasSolution() {

        //Vars: 15
        int DOMAIN = 4;
        List<Constraint> cons = new ArrayList<>();

        cons.add(ConstraintGenerator.createConstraintAllEqual(new int[]{0, 1, 2, 3, 4}, DOMAIN));
        cons.add(ConstraintGenerator.createConstraintAllEqual(new int[]{5, 6, 7, 8, 9}, DOMAIN));
        cons.add(ConstraintGenerator.createConstraintAllEqual(new int[]{10, 11, 12, 13, 14}, DOMAIN));
        cons.add(ConstraintGenerator.createConstraintAllDifferent(new int[]{1, 5, 10}, DOMAIN));

        cons.add(ConstraintGenerator.createBinaryConstraintGreaterThat(12, 9, DOMAIN));
        cons.add(ConstraintGenerator.createBinaryConstraintGreaterThat(5, 1, DOMAIN));

        cons.add(ConstraintGenerator.createUnaryConstraint(0, DOMAIN, new int[]{0, 1}));

        Problem p = new Problem(cons, 15, DOMAIN);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, 20);

        assertEquals(ProblemState.SOLVED, result.problemStatus);
    }

    @Test
    public void testProblem3IsInconsistent() {

        //Vars: 15
        int DOMAIN = 4;
        List<Constraint> cons = new ArrayList<>();

        cons.add(ConstraintGenerator.createConstraintAllEqual(new int[]{0, 1, 2, 3, 4}, DOMAIN));
        cons.add(ConstraintGenerator.createConstraintAllEqual(new int[]{5, 6, 7, 8, 9}, DOMAIN));
        cons.add(ConstraintGenerator.createConstraintAllEqual(new int[]{10, 11, 12, 13, 14}, DOMAIN));
        cons.add(ConstraintGenerator.createConstraintAllDifferent(new int[]{1, 5, 10}, DOMAIN));

        cons.add(ConstraintGenerator.createBinaryConstraintGreaterThat(12, 9, DOMAIN));
        cons.add(ConstraintGenerator.createBinaryConstraintGreaterThat(5, 1, DOMAIN));

        cons.add(ConstraintGenerator.createUnaryConstraint(0, DOMAIN, new int[]{2, 3}));

        Problem p = new Problem(cons, 15, DOMAIN);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, 20);

        assertEquals(ProblemState.INCONSISTENT, result.problemStatus);
    }

    @Test
    public void test10QueensHasSolution() {
        Problem p = ProblemGenerator.createNQueenProblem(10);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, 60);
        assertEquals(ProblemState.SOLVED, result.problemStatus);
    }

    @Test
    public void testSudoku16x16() {
        int BOARD_SIZE = 16;
        int[] reserved = new int[(int) pow(BOARD_SIZE, 2)];

        reserved[0] = 8;
        reserved[19] = 7;
        reserved[22] = 9;
        reserved[24] = 2;
        reserved[40] = 4;
        reserved[48] = 1;
        reserved[42] = 7;
        reserved[252] = 13;
        reserved[65] = 8;
        reserved[173] = 9;
        reserved[61] = 6;
        reserved[62] = 8;
        reserved[70] = 1;
        reserved[66] = 9;

        Problem p = ProblemGenerator.createSudokuProblem(reserved, BOARD_SIZE);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.EWDEG_FWDEGDOM, 600);

        assertEquals(ProblemState.SOLVED, result.problemStatus);
        //System.out.println(result.printResult());
        //AC3 + FwDegDom : Solved {71.84485} sec |  Max depth = 255 | backtracks : {64632} nodes: {74880}
        //AC3 + EwDeg : Solved {1.8528528} sec |  Max depth = 255 | backtracks : {495} nodes: {1015}

        //GAC + FwDegDom : Solved {17.054054} sec |  Max depth = 255 | backtracks : {10970} nodes: {16451}
        //GAC + EwDeg : Solved {1.997998} sec |  Max depth = 255 | backtracks : {131} nodes: {490}

    }

    @Test
    public void testSudoku16x16v2() {
        int BOARD_SIZE = 16;
        int[] reserved = new int[(int) pow(BOARD_SIZE, 2)];

        reserved[0] = 6;
        reserved[2] = 2;
        reserved[5] = 7;
        reserved[6] = 3;
        reserved[9] = 1;
        reserved[12] = 10;
        reserved[15] = 11;

        reserved[16] = 1;
        reserved[18] = 14;
        reserved[23] = 6;
        reserved[24] = 9;
        reserved[30] = 13;
        reserved[31] = 15;

        reserved[36] = 14;
        reserved[37] = 2;
        reserved[38] = 4;
        reserved[40] = 7;
        reserved[42] = 8;
        reserved[44] = 16;
        reserved[45] = 1;
        reserved[46] = 5;

        reserved[50] = 8;
        reserved[51] = 16;
        reserved[54] = 9;
        reserved[58] = 2;
        reserved[59] = 12;
        reserved[62] = 6;

        reserved[65] = 10;
        reserved[67] = 11;
        reserved[68] = 6;
        reserved[69] = 4;
        reserved[71] = 9;
        reserved[72] = 2;
        reserved[73] = 12;
        reserved[77] = 14;

        reserved[80] = 8;
        reserved[82] = 1;
        reserved[83] = 2;
        reserved[85] = 3;
        reserved[88] = 10;
        reserved[90] = 13;
        reserved[91] = 16;
        reserved[92] = 15;
        reserved[93] = 6;
        reserved[94] = 4;
        reserved[95] = 9;

        reserved[96] = 16;
        reserved[97] = 6;
        reserved[98] = 12;
        reserved[99] = 5;
        reserved[100] = 10;
        reserved[101] = 8;
        reserved[105] = 9;
        reserved[107] = 15;
        reserved[109] = 11;
        reserved[111] = 13;

        reserved[113] = 4;
        reserved[114] = 3;
        reserved[116] = 15;
        reserved[117] = 5;
        reserved[123] = 14;
        reserved[125] = 2;

        reserved[130] = 5;
        reserved[133] = 11;
        reserved[135] = 12;
        reserved[138] = 7;
        reserved[139] = 4;
        reserved[141] = 9;
        reserved[143] = 8;

        reserved[144] = 9;
        reserved[145] = 11;
        reserved[151] = 1;
        reserved[153] = 8;
        reserved[156] = 7;
        reserved[158] = 14;
        reserved[159] = 4;

        reserved[160] = 14;
        reserved[162] = 13;
        reserved[169] = 10;
        reserved[170] = 6;
        reserved[171] = 9;
        reserved[173] = 5;
        reserved[175] = 1;

        reserved[177] = 7;
        reserved[178] = 4;
        reserved[179] = 15;
        reserved[182] = 8;
        reserved[183] = 10;

        reserved[192] = 15;
        reserved[194] = 10;
        reserved[200] = 16;
        reserved[203] = 5;
        reserved[204] = 13;

        reserved[208] = 3;
        reserved[209] = 13;
        reserved[210] = 7;
        reserved[211] = 14;
        reserved[213] = 16;
        reserved[214] = 10;
        reserved[217] = 2;
        reserved[219] = 6;
        reserved[220] = 11;
        reserved[222] = 8;

        reserved[225] = 2;
        reserved[233] = 13;
        reserved[234] = 12;
        reserved[236] = 14;
        reserved[237] = 16;
        reserved[238] = 10;
        reserved[239] = 3;

        reserved[242] = 11;
        reserved[243] = 8;
        reserved[246] = 14;
        reserved[251] = 7;
        reserved[253] = 12;
        reserved[254] = 2;

        Problem p = ProblemGenerator.createSudokuProblem(reserved, BOARD_SIZE);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, 600);

        assertEquals(ProblemState.SOLVED, result.problemStatus);
        //System.out.println(result.printResult());
        //AC3 + FwDegDom : Solved {0.7207207} sec |  Max depth = 255 | backtracks : {48} nodes: {311}
        //AC3 + EwDeg : Solved {0.7347347} sec |  Max depth = 255 | backtracks : {3} nodes: {261}

        //GAC + FwDegDom : Solved {1.7137138} sec |  Max depth = 255 | backtracks : {8} nodes: {268}
        //GAC + EwDeg : Solved {1.7317318} sec |  Max depth = 255 | backtracks : {8} nodes: {268}
    }

    @Test
    public void testSudoku16x16v2Incomplete() {
        int BOARD_SIZE = 16;
        int[] reserved = new int[(int) pow(BOARD_SIZE, 2)];

        //Variation of previous test. Missing the last row allows more solutions and more nodes to resolve


        reserved[0] = 6;
        reserved[2] = 2;
        reserved[5] = 7;
        reserved[6] = 3;
        reserved[9] = 1;
        reserved[12] = 10;
        reserved[15] = 11;

        reserved[16] = 1;
        reserved[18] = 14;
        reserved[23] = 6;
        reserved[24] = 9;
        reserved[30] = 13;
        reserved[31] = 15;

        reserved[36] = 14;
        reserved[37] = 2;
        reserved[38] = 4;
        reserved[40] = 7;
        reserved[42] = 8;
        reserved[44] = 16;
        reserved[45] = 1;
        reserved[46] = 5;

        reserved[50] = 8;
        reserved[51] = 16;
        reserved[54] = 9;
        reserved[58] = 2;
        reserved[59] = 12;
        reserved[62] = 6;

        reserved[65] = 10;
        reserved[67] = 11;
        reserved[68] = 6;
        reserved[69] = 4;
        reserved[71] = 9;
        reserved[72] = 2;
        reserved[73] = 12;
        reserved[77] = 14;

        reserved[80] = 8;
        reserved[82] = 1;
        reserved[83] = 2;
        reserved[85] = 3;
        reserved[88] = 10;
        reserved[90] = 13;
        reserved[91] = 16;
        reserved[92] = 15;
        reserved[93] = 6;
        reserved[94] = 4;
        reserved[95] = 9;

        reserved[96] = 16;
        reserved[97] = 6;
        reserved[98] = 12;
        reserved[99] = 5;
        reserved[100] = 10;
        reserved[101] = 8;
        reserved[105] = 9;
        reserved[107] = 15;
        reserved[109] = 11;
        reserved[111] = 13;

        reserved[113] = 4;
        reserved[114] = 3;
        reserved[116] = 15;
        reserved[117] = 5;
        reserved[123] = 14;
        reserved[125] = 2;

        reserved[130] = 5;
        reserved[133] = 11;
        reserved[135] = 12;
        reserved[138] = 7;
        reserved[139] = 4;
        reserved[141] = 9;
        reserved[143] = 8;

        reserved[144] = 9;
        reserved[145] = 11;
        reserved[151] = 1;
        reserved[153] = 8;
        reserved[156] = 7;
        reserved[158] = 14;
        reserved[159] = 4;

        reserved[160] = 14;
        reserved[162] = 13;
        reserved[169] = 10;
        reserved[170] = 6;
        reserved[171] = 9;
        reserved[173] = 5;
        reserved[175] = 1;

        reserved[177] = 7;
        reserved[178] = 4;
        reserved[179] = 15;
        reserved[182] = 8;
        reserved[183] = 10;

        reserved[192] = 15;
        reserved[194] = 10;
        reserved[200] = 16;
        reserved[203] = 5;
        reserved[204] = 13;

        reserved[208] = 3;
        reserved[209] = 13;
        reserved[210] = 7;
        reserved[211] = 14;
        reserved[213] = 16;
        reserved[214] = 10;
        reserved[217] = 2;
        reserved[219] = 6;
        reserved[220] = 11;
        reserved[222] = 8;

        reserved[225] = 2;
        reserved[233] = 13;
        reserved[234] = 12;
        reserved[236] = 14;
        reserved[237] = 16;
        reserved[238] = 10;
        reserved[239] = 3;

//        reserved[242] = 11;
//        reserved[243] = 8;
//        reserved[246] = 14;
//        reserved[251] = 7;
//        reserved[253] = 12;
//        reserved[254] = 2;


        Problem p = ProblemGenerator.createSudokuProblem(reserved, BOARD_SIZE);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.EWDEG_FWDEGDOM, 600);

        assertEquals(ProblemState.SOLVED, result.problemStatus);
        //System.out.println(result.printResult());
        //AC3 + FwDegDom : Solved {22.148148} sec |  Max depth = 255 | backtracks : {16541} nodes: {18766}
        //AC3 + EwDeg : Timeout

        //GAC + FwDegDom : Solved {22.148148} sec |  Max depth = 255 | backtracks : {16541} nodes: {18766}
        //GAC + EwDeg : Solved {4.304304} sec |  Max depth = 255 | backtracks : {998} nodes: {1576}
        System.out.println(result.printResult());
    }

    @Test
    public void testSudokuStd() {

        int[] reserved = new int[81];
        reserved[0] = 8;
        reserved[11] = 3;
        reserved[19] = 7;
        reserved[12] = 6;
        reserved[22] = 9;
        reserved[24] = 2;
        reserved[28] = 5;
        reserved[32] = 7;
        reserved[40] = 4;
        reserved[41] = 5;
        reserved[48] = 1;
        reserved[42] = 7;
        reserved[52] = 3;
        reserved[56] = 1;
        reserved[65] = 8;
        reserved[73] = 9;
        reserved[66] = 5;
        reserved[61] = 6;
        reserved[62] = 8;
        reserved[70] = 1;
        reserved[78] = 4;

        Problem p = ProblemGenerator.createSudokuProblem(reserved, 9);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, 60);
        assertEquals(ProblemState.SOLVED, result.problemStatus);
        //SudokuUtil.printSudokuSolution(result, 9);
        //System.out.println(result.printResult());
    }

    @Test
    public void testSudokuStdUnsat() {

        int[] reserved = new int[81];
        reserved[0] = 8;
        reserved[11] = 3;
        reserved[19] = 7;
        reserved[12] = 6;
        reserved[22] = 9;
        reserved[24] = 2;
        reserved[28] = 5;
        reserved[32] = 7;
        reserved[40] = 4;
        reserved[41] = 5;
        reserved[48] = 1;
        reserved[42] = 7;
        reserved[52] = 3;
        reserved[56] = 1;
        reserved[65] = 8;
        reserved[73] = 9;
        reserved[66] = 5;
        reserved[61] = 6;
        reserved[62] = 8;
        reserved[70] = 1;
        reserved[78] = 4;


        // Adding a value that is not part of the solution. Normally this should be '2'
        // The problem should not be unsat, but should not find a solution either
        reserved[80] = 7;


        //SudokuUtil.printSudokuProblem(reserved, 9);
        Problem p = ProblemGenerator.createSudokuProblem(reserved, 9);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, 60);
        assertEquals(ProblemState.UNSAT, result.problemStatus);
        //System.out.println(result.printResult());
    }

    @Test
    public void testSudokuStdInconsistent() {

        int[] reserved = new int[81];
        reserved[0] = 8;
        reserved[11] = 3;
        reserved[19] = 7;
        reserved[12] = 6;
        reserved[22] = 9;
        reserved[24] = 2;
        reserved[28] = 5;
        reserved[32] = 7;
        reserved[40] = 4;
        reserved[41] = 5;
        reserved[48] = 1;
        reserved[42] = 7;
        reserved[52] = 3;
        reserved[56] = 1;
        reserved[65] = 8;
        reserved[73] = 9;
        reserved[66] = 5;
        reserved[61] = 6;
        reserved[62] = 8;
        reserved[70] = 1;
        reserved[78] = 4;

        // This should make the problem initially inconsistent, as there is another '9' in that row
        reserved[80] = 9;

        //SudokuUtil.printSudokuProblem(reserved, 9);
        Problem p = ProblemGenerator.createSudokuProblem(reserved, 9);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, 60);
        assertEquals(ProblemState.INCONSISTENT, result.problemStatus);
        //System.out.println(result.printResult());
    }

    @Test
    public void testProblemWithCustomTuples() {
        //This problem will start by assigning x3 a value then try another value. Tests problems related to backtracking in depth = 0

        List<Constraint> constraints = new ArrayList<>();

        Constraint c1 = new Constraint(2, 3);
        c1.tuples[0] = 1;
        c1.tuples[1] = 1;
        c1.tuples[2] = 1;
        c1.tuples[3] = 1;
        c1.tuples[4] = 0;
        c1.tuples[5] = 0;
        c1.tuples[6] = 0;
        c1.tuples[7] = 0;
        c1.tuples[8] = 0;
        c1.varGlobalIDs = new int[]{4, 2};
        constraints.add(c1);

        Constraint c2 = new Constraint(2, 3);
        c2.tuples[0] = 1;
        c2.tuples[1] = 0;
        c2.tuples[2] = 0;
        c2.tuples[3] = 1;
        c2.tuples[4] = 0;
        c2.tuples[5] = 0;
        c2.tuples[6] = 0;
        c2.tuples[7] = 1;
        c2.tuples[8] = 1;
        c2.varGlobalIDs = new int[]{1, 3};
        constraints.add(c2);

        Constraint c3 = new Constraint(2, 3);
        c3.tuples[0] = 1;
        c3.tuples[1] = 0;
        c3.tuples[2] = 0;
        c3.tuples[3] = 0;
        c3.tuples[4] = 0;
        c3.tuples[5] = 1;
        c3.tuples[6] = 0;
        c3.tuples[7] = 1;
        c3.tuples[8] = 1;
        c3.varGlobalIDs = new int[]{2, 1};
        constraints.add(c3);

        Constraint c4 = new Constraint(2, 3);
        c4.tuples[0] = 0;
        c4.tuples[1] = 0;
        c4.tuples[2] = 1;
        c4.tuples[3] = 1;
        c4.tuples[4] = 1;
        c4.tuples[5] = 0;
        c4.tuples[6] = 0;
        c4.tuples[7] = 0;
        c4.tuples[8] = 1;
        c4.varGlobalIDs = new int[]{1, 0};
        constraints.add(c4);

        Constraint c5 = new Constraint(2, 3);
        c5.tuples[0] = 1;
        c5.tuples[1] = 0;
        c5.tuples[2] = 1;
        c5.tuples[3] = 1;
        c5.tuples[4] = 0;
        c5.tuples[5] = 0;
        c5.tuples[6] = 1;
        c5.tuples[7] = 0;
        c5.tuples[8] = 0;
        c5.varGlobalIDs = new int[]{0, 3};
        constraints.add(c5);

        Problem p = new Problem(constraints, 5, 3);
        Solver s = SolverFactory.getDefaultsolver(p);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, 10);

        assertEquals(ProblemState.SOLVED, result.problemStatus);
    }

}
