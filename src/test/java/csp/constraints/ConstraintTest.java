package csp.constraints;

import csp.solvers.Solver;
import csp.solvers.SolverFactory;
import csp.exceptions.CSPException;
import csp.problems.Problem;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;

public class ConstraintTest {

    @Test
    public void testRandomization() {
        Constraint a = new Constraint(3, 3);
        int targetAllowedTuples = (int) Math.round((1.0 - 0.7) * Math.pow(3, 3));
        a.randomiseAllowedTuples(targetAllowedTuples, 0);

        int totalAvailableTuples = 0;
        for (int i = 0; i < a.tuples.length; i++) {
            totalAvailableTuples += a.tuples[i];
        }
        int expected = (int) (27 * (1 - 0.7));
        assertEquals(totalAvailableTuples, expected);
    }

    @Test
    public void testGetVarValueInTuple() {
        Constraint c = prepareConstraintExample1();

        assertEquals(c.getVarValueInTuple(0, 20), 2);
        assertEquals(c.getVarValueInTuple(1, 12), 1);
        assertEquals(c.getVarValueInTuple(2, 18), 0);
        assertEquals(c.getVarValueInTuple(0, 0), 0);
        assertEquals(c.getVarValueInTuple(2, 26), 2);
    }


    //@Test
    public void testCountSupports() {
        Constraint c = prepareConstraintExample1();

        assertEquals(c.countSupports(0, 1), 2);
        assertEquals(c.countSupports(2, 0), 3);
        assertEquals(c.countSupports(1, 2), 4);
    }

    @Test
    public void testCountSupportsDynamic() {
        Constraint c = prepareConstraintExample1();

        List<Constraint> cons = new ArrayList<>();
        cons.add(c);

        Problem p = new Problem(cons, 22, 3);
        Solver s = SolverFactory.getDefaultsolver(p);

        c.varGlobalIDs[0] = 0;
        c.varGlobalIDs[1] = 1;
        c.varGlobalIDs[2] = 2;

        assertEquals(2, c.countSupportsDynamic(0, 0, s));
        assertEquals(2, c.countSupportsDynamic(0, 1, s));
        assertEquals(4, c.countSupportsDynamic(0, 2, s));

        assertEquals(2, c.countSupportsDynamic(1, 0, s));
        assertEquals(2, c.countSupportsDynamic(1, 1, s));
        assertEquals(4, c.countSupportsDynamic(1, 2, s));

        assertEquals(3, c.countSupportsDynamic(2, 0, s));
        assertEquals(2, c.countSupportsDynamic(2, 1, s));
        assertEquals(3, c.countSupportsDynamic(2, 2, s));


        c.varGlobalIDs[0] = 7;
        c.varGlobalIDs[1] = 21;
        c.varGlobalIDs[2] = 1;

        assertEquals(2, c.countSupportsDynamic(7, 0, s));
        assertEquals(2, c.countSupportsDynamic(7, 1, s));
        assertEquals(4, c.countSupportsDynamic(7, 2, s));

        assertEquals(2, c.countSupportsDynamic(21, 0, s));
        assertEquals(2, c.countSupportsDynamic(21, 1, s));
        assertEquals(4, c.countSupportsDynamic(21, 2, s));

        assertEquals(3, c.countSupportsDynamic(1, 0, s));
        assertEquals(2, c.countSupportsDynamic(1, 1, s));
        assertEquals(3, c.countSupportsDynamic(1, 2, s));
    }

    @Test
    public void testCountSupportsDynamicWithUnsupportedValues() {
        Constraint c = prepareConstraintExample2();

        List<Constraint> cons = new ArrayList<>();
        cons.add(c);

        Problem p = new Problem(cons, 22, 3);
        Solver s = SolverFactory.getDefaultsolver(p);

        c.varGlobalIDs[0] = 7;
        c.varGlobalIDs[1] = 21;
        c.varGlobalIDs[2] = 1;

        // Let x7 = 0, x1 = 2
        s.markAsSupported(7,0);
        s.markAsUnsupported(7,1, 7, 0);
        s.markAsUnsupported(7,2, 7, 0);


        s.markAsSupported(21,0);
        s.markAsSupported(21,1);
        s.markAsSupported(21,2);

        s.markAsUnsupported(1,0,1,2);
        s.markAsUnsupported(1,1,1,2);
        s.markAsSupported(1,2);


        assertEquals(2, c.countSupportsDynamic(7, 0, s));
        assertEquals(0, c.countSupportsDynamic(7, 1, s));
        assertEquals(0, c.countSupportsDynamic(7, 2, s));

        assertEquals(1, c.countSupportsDynamic(21, 0, s));
        assertEquals(0, c.countSupportsDynamic(21, 1, s));
        assertEquals(1, c.countSupportsDynamic(21, 2, s));

        assertEquals(0, c.countSupportsDynamic(1, 0, s));
        assertEquals(0, c.countSupportsDynamic(1, 1, s));
        assertEquals(2, c.countSupportsDynamic(1, 2, s));
    }

    @Test
    public void testContainsVar() {
        Constraint c = prepareConstraintExample1();
        c.varGlobalIDs[0] = 5;
        c.varGlobalIDs[1] = 271;
        c.varGlobalIDs[2] = 3;

        assertEquals(c.containsVar(5), true);
        assertEquals(c.containsVar(271), true);
        assertEquals(c.containsVar(3), true);
        assertEquals(c.containsVar(-1), false);
        assertEquals(c.containsVar(8), false);
    }

    @Test
    public void testGetLocalIdForVariable() {
        Constraint c = prepareConstraintExample1();
        c.varGlobalIDs[0] = 5;
        c.varGlobalIDs[1] = 271;
        c.varGlobalIDs[2] = 3;

        assertEquals(c.getLocalIdForVariable(5), 0);
        assertEquals(c.getLocalIdForVariable(271), 1);
        assertEquals(c.getLocalIdForVariable(3), 2);
    }

    @Test(expected = CSPException.class)
    public void testGetLocalIdForVariableWithUnknownVarThrowsException() {
        Constraint c = prepareConstraintExample1();
        c.varGlobalIDs[0] = 5;
        c.varGlobalIDs[1] = 271;
        c.varGlobalIDs[2] = 3;

        c.getLocalIdForVariable(58);
    }

    @Test
    public void testIsSatisfied() {
        Constraint c = prepareConstraintExample1();

        int[] assignment1 = {2, 2, 1};
        int[] assignment2 = {1, 0, 2};
        int[] assignment3 = {2, 2, -1};
        int[] assignment4 = {0, -1, 1};

        assertTrue(c.isSatisfied(assignment1));
        assertFalse(c.isSatisfied(assignment2));
        assertTrue(c.isSatisfied(assignment3));
        assertFalse(c.isSatisfied(assignment4));
    }

    static Constraint prepareConstraintExample1() {
        Constraint c = new Constraint(3, 3);

        int[] tuples = new int[27];
        int[] varIDs = new int[3];


        tuples[0] = 0;
        tuples[1] = 0;
        tuples[2] = 1;
        tuples[3] = 0;
        tuples[4] = 0;
        tuples[5] = 0;
        tuples[6] = 1;
        tuples[7] = 0;
        tuples[8] = 0;
        tuples[9] = 0;

        tuples[10] = 0;
        tuples[11] = 0;
        tuples[12] = 1;
        tuples[13] = 1;
        tuples[14] = 0;
        tuples[15] = 0;
        tuples[16] = 0;
        tuples[17] = 0;
        tuples[18] = 0;
        tuples[19] = 0;

        tuples[20] = 1;
        tuples[21] = 0;
        tuples[22] = 0;
        tuples[23] = 0;
        tuples[24] = 1;
        tuples[25] = 1;
        tuples[26] = 1;

        varIDs[0] = 0;
        varIDs[1] = 1;
        varIDs[2] = 2;

        c.setConstraintTuples(tuples);
        c.setVarIds(varIDs);

        return c;

        /*

        tuple| Var | allowed
        ---------------------
         0   0,0,0, : 0
         1   0,0,1, : 0
         2   0,0,2, : 1
         3   0,1,0, : 0
         4   0,1,1, : 0
         5   0,1,2, : 0
         6   0,2,0, : 1
         7   0,2,1, : 0
         8   0,2,2, : 0
         9   1,0,0, : 0
         10  1,0,1, : 0
         11  1,0,2, : 0
         12  1,1,0, : 1
         13  1,1,1, : 1
         14  1,1,2, : 0
         15  1,2,0, : 0
         16  1,2,1, : 0
         17  1,2,2, : 0
         18  2,0,0, : 0
         19  2,0,1, : 0
         20  2,0,2, : 1
         21  2,1,0, : 0
         22  2,1,1, : 0
         23  2,1,2, : 0
         24  2,2,0, : 1
         25  2,2,1, : 1
         26  2,2,2, : 1
         */
    }

    static Constraint prepareConstraintExample2() {
        Constraint c = new Constraint(3, 3);

        int[] tuples = new int[27];
        int[] varIDs = new int[3];

        tuples[0] = 0;
        tuples[1] = 1;
        tuples[2] = 1;
        tuples[3] = 0;
        tuples[4] = 0;
        tuples[5] = 0;
        tuples[6] = 1;
        tuples[7] = 0;
        tuples[8] = 1;
        tuples[9] = 0;

        tuples[10] = 0;
        tuples[11] = 0;
        tuples[12] = 1;
        tuples[13] = 1;
        tuples[14] = 0;
        tuples[15] = 0;
        tuples[16] = 0;
        tuples[17] = 1;
        tuples[18] = 0;
        tuples[19] = 0;

        tuples[20] = 1;
        tuples[21] = 0;
        tuples[22] = 0;
        tuples[23] = 0;
        tuples[24] = 1;
        tuples[25] = 1;
        tuples[26] = 0;


        varIDs[0] = 0;
        varIDs[1] = 1;
        varIDs[2] = 2;

        c.setConstraintTuples(tuples);
        c.setVarIds(varIDs);

        return c;

        /*

        tuple| Var | allowed
        ---------------------
         0   0,0,0, : 0
         1   0,0,1, : 0
         2   0,0,2, : 1
         3   0,1,0, : 0
         4   0,1,1, : 0
         5   0,1,2, : 0
         6   0,2,0, : 1
         7   0,2,1, : 0
         8   0,2,2, : 0
         9   1,0,0, : 0
         10  1,0,1, : 0
         11  1,0,2, : 0
         12  1,1,0, : 1
         13  1,1,1, : 1
         14  1,1,2, : 0
         15  1,2,0, : 0
         16  1,2,1, : 0
         17  1,2,2, : 0
         18  2,0,0, : 0
         19  2,0,1, : 0
         20  2,0,2, : 1
         21  2,1,0, : 0
         22  2,1,1, : 0
         23  2,1,2, : 0
         24  2,2,0, : 1
         25  2,2,1, : 1
         26  2,2,2, : 1
         */
    }


}
