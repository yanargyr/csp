package csp.constraints;


import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ConstraintGeneratorTest {

    @Test
    public void testUnaryConstraintHasCorrectTupleValues() {
        Constraint c = ConstraintGenerator.createUnaryConstraint(6, 10, new int[]{3, 5, 8});

        assertEquals(0, c.tuples[0]);
        assertEquals(0, c.tuples[1]);
        assertEquals(0, c.tuples[2]);
        assertEquals(1, c.tuples[3]);
        assertEquals(0, c.tuples[4]);
        assertEquals(1, c.tuples[5]);
        assertEquals(0, c.tuples[6]);
        assertEquals(0, c.tuples[7]);
        assertEquals(1, c.tuples[8]);
        assertEquals(0, c.tuples[9]);

        assertEquals(6, c.varGlobalIDs[0]);
    }

    @Test
    public void testBinaryGreaterHasCorrectTupleValues() {
        Constraint c = ConstraintGenerator.createBinaryConstraintGreaterThat(3, 9, 3);

        assertEquals(0, c.tuples[0]);
        assertEquals(0, c.tuples[1]);
        assertEquals(0, c.tuples[2]);
        assertEquals(1, c.tuples[3]);
        assertEquals(0, c.tuples[4]);
        assertEquals(0, c.tuples[5]);
        assertEquals(1, c.tuples[6]);
        assertEquals(1, c.tuples[7]);
        assertEquals(0, c.tuples[8]);

        assertEquals(3, c.varGlobalIDs[0]);
        assertEquals(9, c.varGlobalIDs[1]);
    }

    @Test
    public void testBinaryConstraintRestrictPairOfValues() {
        Constraint c = ConstraintGenerator.createBinaryConstraintRestrictPairOfValues(0, 1, 2, 1, 4);
        for (int i = 0; i < Math.pow(4, 2); i++) {
             if (i == 9) {
                assertEquals(0, c.tuples[i]);
            } else {
                assertEquals(1, c.tuples[i]);
            }
        }
    }

    @Test
    public void testBinaryGreaterOrEqualsHasCorrectTupleValues() {
        Constraint c = ConstraintGenerator.createBinaryConstraintGreaterThatOrEquals(3, 9, 3);

        assertEquals(1, c.tuples[0]);
        assertEquals(0, c.tuples[1]);
        assertEquals(0, c.tuples[2]);
        assertEquals(1, c.tuples[3]);
        assertEquals(1, c.tuples[4]);
        assertEquals(0, c.tuples[5]);
        assertEquals(1, c.tuples[6]);
        assertEquals(1, c.tuples[7]);
        assertEquals(1, c.tuples[8]);

        assertEquals(3, c.varGlobalIDs[0]);
        assertEquals(9, c.varGlobalIDs[1]);
    }

    @Test
    public void testAllDiffHasCorrectTupleValues() {
        Constraint c = ConstraintGenerator.createConstraintAllDifferent(new int[]{5,10,2}, 3);

        assertEquals(0, c.tuples[0]);
        assertEquals(0, c.tuples[1]);
        assertEquals(0, c.tuples[2]);
        assertEquals(0, c.tuples[3]);
        assertEquals(0, c.tuples[4]);
        assertEquals(1, c.tuples[5]);
        assertEquals(0, c.tuples[6]);
        assertEquals(1, c.tuples[7]);
        assertEquals(0, c.tuples[8]);
        assertEquals(0, c.tuples[9]);

        assertEquals(0, c.tuples[10]);
        assertEquals(1, c.tuples[11]);
        assertEquals(0, c.tuples[12]);
        assertEquals(0, c.tuples[13]);
        assertEquals(0, c.tuples[14]);
        assertEquals(1, c.tuples[15]);
        assertEquals(0, c.tuples[16]);
        assertEquals(0, c.tuples[17]);
        assertEquals(0, c.tuples[18]);
        assertEquals(1, c.tuples[19]);

        assertEquals(0, c.tuples[20]);
        assertEquals(1, c.tuples[21]);
        assertEquals(0, c.tuples[22]);
        assertEquals(0, c.tuples[23]);
        assertEquals(0, c.tuples[24]);
        assertEquals(0, c.tuples[25]);
        assertEquals(0, c.tuples[26]);

        assertEquals(5, c.varGlobalIDs[0]);
        assertEquals(10, c.varGlobalIDs[1]);
        assertEquals(2, c.varGlobalIDs[2]);
    }


    @Test
    public void testAllEqualHasCorrectTupleValues() {
        Constraint c = ConstraintGenerator.createConstraintAllEqual(new int[]{5,10,2}, 3);

        assertEquals(1, c.tuples[0]);
        assertEquals(0, c.tuples[1]);
        assertEquals(0, c.tuples[2]);
        assertEquals(0, c.tuples[3]);
        assertEquals(0, c.tuples[4]);
        assertEquals(0, c.tuples[5]);
        assertEquals(0, c.tuples[6]);
        assertEquals(0, c.tuples[7]);
        assertEquals(0, c.tuples[8]);
        assertEquals(0, c.tuples[9]);

        assertEquals(0, c.tuples[10]);
        assertEquals(0, c.tuples[11]);
        assertEquals(0, c.tuples[12]);
        assertEquals(1, c.tuples[13]);
        assertEquals(0, c.tuples[14]);
        assertEquals(0, c.tuples[15]);
        assertEquals(0, c.tuples[16]);
        assertEquals(0, c.tuples[17]);
        assertEquals(0, c.tuples[18]);
        assertEquals(0, c.tuples[19]);

        assertEquals(0, c.tuples[20]);
        assertEquals(0, c.tuples[21]);
        assertEquals(0, c.tuples[22]);
        assertEquals(0, c.tuples[23]);
        assertEquals(0, c.tuples[24]);
        assertEquals(0, c.tuples[25]);
        assertEquals(1, c.tuples[26]);

        assertEquals(5, c.varGlobalIDs[0]);
        assertEquals(10, c.varGlobalIDs[1]);
        assertEquals(2, c.varGlobalIDs[2]);
    }
}
