package csp.problems;

import csp.solvers.Solver;
import csp.solvers.SolverFactory;
import csp.constraints.Constraint;
import csp.data.SolverResult;
import csp.enums.HeuristicStrategy;
import csp.enums.ProblemState;
import org.junit.Assert;
import org.junit.Test;

public class RandomProblemTest {

    @Test
    public void testCreatesRandomProblemWithCorrectNumberOfConstraintsAndAllowedTuples() {

        int vars = 4;
        int min_arity = 3;
        int max_arity = 3;
        double p = 0.50;
        double q = 0.2;
        int domainSize = 5;
        int timeLimit = 6;
        int seed = 5;

        Problem pr = new RandomProblem(vars, min_arity, max_arity, p, q, domainSize, seed);
        Solver s = SolverFactory.getDefaultsolver(pr);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, timeLimit);

        int expectedConstraints = 2; // 0.5 * (  vars!/(arity!*(vars-arity)!)) = 0.5 * 4 = 2
        int expectedAllowedTuples = 200; // expectedConstraints * floor((1- q) * domainSize^arity) = 2 * floor(0.8 * 5^3) = 2 * 100 = 200


        int countedAllowedTuples = 0;
        for (int i = 0; i < pr.totalConstraints; i++) {

            Constraint c = pr.C[i];
            for (int j = 0; j < (int) Math.pow(domainSize, max_arity); j++) {
                countedAllowedTuples += c.tuples[j];
            }
        }

        Assert.assertEquals(ProblemState.SOLVED, result.problemStatus);
        Assert.assertEquals(expectedConstraints, pr.C.length);
        Assert.assertEquals(expectedAllowedTuples, countedAllowedTuples);
    }

    @Test
    public void testCreatesRandomProblemWithCorrectNumberOfConstraintsAndAllowedTuples2() {

        int vars = 6;
        int min_arity = 2;
        int max_arity = 2;
        double p = 0.75;
        double q = 0.1;
        int domainSize = 3;
        int timeLimit = 6;
        int seed = 9;

        Problem pr = new RandomProblem(vars, min_arity, max_arity, p, q, domainSize, seed);
        Solver s = SolverFactory.getDefaultsolver(pr);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, timeLimit);

        int expectedConstraints = 11; // 0.75 * (  vars!/(arity!*(vars-arity)!)) = 0.75 * 15 = 11.25 -> 11
        int expectedAllowedTuples = 88; // expectedConstraints * floor((1- q) * domainSize^arity) = 11 * floor(0.9 * 3^2) = 11 * 8 = 88

        int countedAllowedTuples = 0;
        for (int i = 0; i < pr.totalConstraints; i++) {

            Constraint c = pr.C[i];
            for (int j = 0; j < (int) Math.pow(domainSize, max_arity); j++) {
                countedAllowedTuples += c.tuples[j];
            }
        }

        Assert.assertEquals(ProblemState.SOLVED, result.problemStatus);
        Assert.assertEquals(expectedConstraints, pr.C.length);
        Assert.assertEquals(expectedAllowedTuples, countedAllowedTuples);
    }

    @Test
    public void testRandomProblemWithMultipleAritiesHasCorrectNumberOfConstraintsAndAllowedTuples() {

        int vars = 6;
        int min_arity = 2;
        int max_arity = 4;
        double p = 0.2;
        double q = 0.5;
        int domainSize = 4;
        int timeLimit = 6;
        int seed = 1;

        Problem pr = new RandomProblem(vars, min_arity, max_arity, p, q, domainSize, seed);
        Solver s = SolverFactory.getDefaultsolver(pr);
        SolverResult result = s.solve(HeuristicStrategy.FWDEGDOM, timeLimit);

        /* Constraint to create:
            arity = 2 : 6!/ 2!*4! = 720 / 2*24 = 15 (max possible)
             15 * 0.2 = 3
            arity = 3 : 6!/ 3!*3! = 720 / 6*6 = 20
              20 * 0.2 = 4
            arity = 4 : 6!/ 4!*2! = 720 / 24*2 = 15
              15 * 0.2 = 3
            Total = 3 + 4 + 3 = 10
         */

        int expectedConstraints = 10;


        /* Tuples to Create:
        arity = 2 :
            allowed tuples / constraint = 0.5 * 4^2 = 8
            total : 3 x 8 = 24
        arity = 3 :
            allowed tuples / constraint = 0.5 * 4^3 = 32
            total : 4 x 32 = 128
        arity = 4 :
            allowed tuples / constraint = 0.5 * 4^4 = 128
            total : 3 x 128 = 384

         total tuples : 536

        */

        int expectedAllowedTuples = 536;

        int countedAllowedTuples = 0;
        for (int i = 0; i < pr.totalConstraints; i++) {
            Constraint c = pr.C[i];
            for (int j = 0; j < (int) Math.pow(domainSize, c.arity); j++) {
                countedAllowedTuples += c.tuples[j];
            }
        }


        Assert.assertEquals(ProblemState.SOLVED, result.problemStatus); // Solution might be affected by random seed.
        Assert.assertEquals(expectedConstraints, pr.C.length);
        Assert.assertEquals(expectedAllowedTuples, countedAllowedTuples);
    }

}
