
# 1. Introduction
This is a Constraint Satisfaction Problem (CSP) solver, created for my MSc thesis, "SAT-Based CSP Heuristics", supervised by Dr Kostas Stergiou, and also used for our research "A study of SAT-based branching heuristics for the CSP (SETN 2008)".
The subject of this work was the adoption of SAT-based variable ordering heuristics, for the CSP problem, as these heuristics are using information like constraint arity, and value supports.
However this CSP solver is using the basic CSP framework principles and should be extendable for more advanced techniques.
In it's initial form, this solver used to work with random CSP problems, but I have done some work on making it work with real problems as well

- Author: Yannis Argyropoulos
- contact yanargyr@gmail.com

# 2. Solver features

## 2.1 Definition and terminology
A CSP is defined as a triple `(X,D,C)`, where 
- `X = {x1,x2,..xn}` is a finite set of `n` **variables**,
- `D = {D(x1), D(x2),… D(xn)}` is the set of their respective (finite) **domains**,
- `C` is a set of **constraints**.


- For any constraint `c`, on `m` variables `x1,x2,..xm`:
    - `vars(c)` denotes the variables involved in it
    - `rel(c) ⊆ D(x1)×D(x2) ×..× D(xm)` is the set of combinations (or tuples) of assignments for the variables `x1,x2,..xm` that satisfy the constraint.
    - A *tuple* `T∈rel(c)` is called valid when all the values assigned to the respective variables `x1,x2,..xm∈vars(c)` are available in the corresponding domains.

- For any variable x,
    - `|dom(x)|` denotes the cardinality of the variable’s current domainSize and
    - *Forward Degree* (`fwdeg`) denoted the number of constraints with unassigned variables where `x` is involved in.

- The `arity` of a constraint `c` is the number of variables involved in `c`.
The current arity of `c` is the number of unassigned variables in `c`.
- A binary constraint `c` between variables `xi` and `xj` is *arc consistent* (AC) `iff` for any value `a in D(xi)` there exists at least one value `b in D(xj)` so that `a` and `b` satisfy `c`, and vice versa.
In this case `b` is called a *support of `a` on `c`*. This definition has been extended to non-binary constraints, giving generalized arc consistency (GAC).

## 2.2. Search and Arc Consistency

This solver is using Depth First Search, Chronological Backtracking and Maintain Arc Consistency (MAC) strategy to make a complete search. After each variable assignment, the solver will check for arc consistency
and propagate constraints, reducing the domains of unassigned variables. Every time an Arc-Inconsistency is detected, the solver will backtrack. At first it will try other values for the same variable. 
If all values are tried unsuccessfully in a tree search depth, it will backtrack to the previous depth and try another variable. If all values of a variable are tried in the first node of the search tree (depth = 0),
then the problem has no solution

The solver has been using an implementation of AC3, that also worked for non-binary CSPs. But after my academic research work, I've implemented another version of GAC. 
In practice it is a bit slower, but is also much more effective when it comes to pruning, especially for hard random problems or real problems.
As a rule of thumb, the Arc Consistency check pays off by reducing the search tree as it's reducing total backtracks and assignments,.


### 2.2.1 .Solver Input
The basic input for a real problem is:

- A finite integer number of variables. For example, for totalVars = 5, then vars x0, x1, x2, x3 and x4 will be created

- A finite list of constraints. The constraints can be of arity 1 or 2 or more. The constraints are not defined in a descriptive form (e.g. x0 > x2), but by the set of their valid tuples

- A uniform domainSize size of finite values, i.e every variable starts with the same domainSize.

- The heuristic method to use for branching

- A timeout limit to stop the solver

Extra input for a random Problem

- Minimum constraint arity

- Maximum constraint arity

- Probability to create a constraint. Range in `[0, 1]`. Bigger means more constraints
    For a problem with V variables and Constraints of arity A, you can have up to `V! / (A! * (V-A)!)` total constraints
    
- Constraint tightness (`q`). It represents the proportion of non-valid tuples in each constraint. Bigger means tighter (fewer allowed tuples)
    If a constraint involves 3 variables, and domainSize size = 4, then it will have 4^3 tuples
    If `q = 0.25`, then you will get a constraint with `4^3 * (0.25)` = *`16` non valid tuples*
        - and `4^3 * (1 - 0.25)` = *`48` valid tuples*
    For compatibility with with other research, sometimes the constraint Looseness is instead (1-q instead of q)

- And integer seed for the randomisation of the constraints

### 2.2.2 Output
The Solver will try to find one solution, if there is one. If there is a solution, it will also return the assignment that solves it. If more than one solutions exist, it will just return the first solution it found
There are 4 different outcomes for this solver:
- ***Solved***: There is at least one solution
- ***Unsatisfiable***: There is no solution, as there is no possible assignment that can satisfy every problem constraint
- ***Inconsistent***: The problem is not arc consistent. This is a special case of the 'Unsatisfiable' category, as in this case there was no need to try any assignments at all
- ***Timed Out***: The solved timed out before finishing. There might might be a solution. Or not


### 2.2.3. Uniform Domain
All variables are using a common domainSize, and the domainSize is supposed to be a sequence of 0-indexed integers. 
If the domainSize size is 5, then all variables will have this domainSize : `{0,1,2,3,4}`

If you need to solve a problem where the domainSize is not integer values (e.g. letters, colors, city names, negative numbers etc), you need to map each value to an integer. 

If you need to solve a problem where each var has a different domainSize, you can still do it with a workaround, you can concatenate all unique values in one set, and add unary constraints to limit each variable's domainSize

Example:
if `D(x0) = {0,1,4}, D(x1) = {2,3}, D(x2) = {0,2,5}`
Then you can declare this problem as:
- Total Vars: `4`
- Domain size: `6` (vars will start with a common domainSize : `{0,1,2,3,4,5}`)
- Add one unary constraint for each variable e.g:
     > `ConstraintGenerator.createUnaryConstraint(0, 6, new int[]{0,1,4})`
     `ConstraintGenerator.createUnaryConstraint(1, 6, new int[]{2,3})`
     `ConstraintGenerator.createUnaryConstraint(2, 6, new int[]{0,2,5})`


### 2.2.4 Real Constraint types
Using the `ConstraintGenerator` you can create the following types of constraints

- Unary Constraints
    - Restricting variable domainSize 
    > `ConstraintGenerator.createUnaryConstraint(varGlobalID, domainSize, allowedValues)`
- Binary Constraints
    - Restricting pair of values (`!(x0=2 ^ x1=3)`) 
    > `ConstraintGenerator.createBinaryConstraintRestrictPairOfValues(0, 1, 2, 3, domainSize)`
    - Less than (`x0 < x1`)
    > `ConstraintGenerator.createBinaryConstraintGreaterThat(0, 1, domainSize)`
    - Less or Equal (`x0 <= x1`)
    > `ConstraintGenerator.createBinaryConstraintGreaterThatOrEquals(0, 1, domainSize)`
- Constraints of bigger arity (2 or more)
    - All Different (`x0 != x1 ^ x0 != x2 ^ x1 != 2`) 
    > `ConstraintGenerator.createConstraintAllDifferent(new int[]{0,1,2}, domainSize)`
    - All Equal (`x0 = x1 = x2`)
    > `ConstraintGenerator.createConstraintAllEqual(new int[]{0,1,2} domainSize)`

# 3. Heuristics
Here goes a quick summary of the best performing heuristics implemented for this solver

## 3.1 FwDeg/Dom
For each variable, takes into account it's current domainSize size and forward degree. 
This results in picking a variable that does not have many values left to try (therefore adds less branches), and propagates pruning to as many constraints as possible.
This is one of the most common MAC heuristics, has good performance in almost all classes of problems, and is also light weight
> Ref : Bessiere C., Regin J., “Mac and Combined Heuristics: two Reasons to Forsake FC (and CBJ?) on Hard Problems”, 1996.

        
        
## 3.2 Jeroslow-Wang for CSP
This is SAT-framework heuristic that selects variables involved in clauses of minimal length. This way it manages to prune other variable's domains faster.
In  it's CSP adaption, it takes into account each variable's forward Degree, it's current domainSize, and also uses a larger weight to variables involved in constraints of smaller dynamic arity.
It performs similarly to FwDeg/Dom. In Binary Constraint problems, it makes exactly the same assignments, but costs a little bit more to execute.
When constraint arities are larger than 2, then this one starts performing better.
> Ref: Jeroslow, R.J., Wang, J.: Solving propositional satisfiability problems, Annals of Mathematics and Artificial Intelligence 1, 167 – 188 (1990). 12

## 3.3 Extended Weight Degree
An extension to the weighted degree heuristics that takes into account information based on 
 1) the current arity of the constraints 
 2) the current domainSize sizes of the unassigned variables in the constraints. 
 3) the number of times the constraint has caused a Domain Wipe Out
 > Ref: Yannis Argyropoulos, Kostas Stergiou: A study of SAT-based branching heuristics for the CSP, SETN 2008 (attached) 

#Notes
##4.1 Random Problem generation benchmarking and referenced problem classes
Since the release of the experimental results (see attached), there have been improvements around the randomisation algorithm:
- More accurate population of total constraints and allowed tuples, especially in problems with multiple arities.
- Different (randomised) constraint tightness per constraint, to demonstrate weighted degree heuristics more accurately.

Hence, the same problem classes are not populating the same proportions of sat/unsat problems as before. 

Usually, for the same class of problem params (vars, arity, domain size, constraint tightness), the problems should be less likely to be initially consistent, or satisfiable.
Although all constraints will have the same average allowed tuples as before, the ones that are below average will be tighter, so the problem will be tighter too. 
 
## 4.2 Notes on performance and usage
While this CSP solver supports non-binary constraints, in practice there are memory/computing limitations caused by the representation of constraints through tuples.
For a constraint that involves 9 variables, and domainSize size is 9, there would be 9^9 tuples.
While it is OK to create constraints with arities of 3, 4 or 5 with domainSize size = 10, you will see issues with higher arities.
It is better to "break" these constraints to binary constraints using permutation.
For example. the constraint `x1=x2=x3=x4` is equivalent to
`x1=x2 ^ x1=x3 ^ x1=x4 ^ x2=x3 ^ x2=x4 ^ x3=x4`

## 4.3 Build and use
Build the project and build a jar file by
 
1) Opening a command/powershell/bash terminal inside the project root

2) Execute either `mvn install` or `mvn package`
This will create a new jar under the `target/` directory. You can import this into your project, or run a demo from the command line
`$ java -jar target/CSP-Engine-1.0.jar`

 
# 5. References: 
>
    0. Yannis Argyropoulos, Kostas Stergiou: A study of SAT-based branching heuristics for the CSP, SETN 2008 38-50(attached)       
    1. Bessiere, C., Regin, J.: Mac and Combined Heuristics: two Reasons to Forsake FC (and CBJ?) on Hard Problems, CP (1996). 
    2. Bordeaux, L., Hamadi, Y., Zhang, L.: Propositional Satisfiability and Constraint Programming: A Comparative Survey, ACM Computing Surveys, 38(4) (2006). 
    3. Buro, M., Kleine-Buning, H.: Report of a SAT competition, Technical Report, University of Paderborn, (1992). 
    4. Boussemart, F., Hemery, F., Lecoutre, C., Lakhdar, S.: Boosting systematic search by weighting contraints, ECAI (2004)
    5. Davis, M., Logemann, G., and Loveland D.: A machine program for theorem-proving, Communications of the ACM 5, 7, 393 – 397 (1962). 
    6. Dechter, R., Meiri, I.: Experimental evaluation of preprocessing algorithms for constraint satisfaction problems, Artificial Intelligence, 68: 211 – 241 (1994). 
    7. Freeman, J.W.: Improvements to propositional satisfiability search algorithms, Ph.D. thesis, Department of Computer and Information Science, University of Pennsylvania (1995). 
    8. Geelen, P.A.: Dual viewpoint heuristics for binary constraint satisfaction problems, ECAI ‘92, 31 – 35, Vienna, Austria (1992). 
    9. Grimes, D., Wallace, R.J.: Sampling Strategies and Variable Selection in Weighted Degree Heuristics, 831-838, CP (2007). 
    10. Haralick, R.M., Elliot, G.L.: Increasing tree search efficiency for constraint satisfaction problems, Artificial Intelligence, 14: 263 – 313 (1980). 
    11. Jeroslow, R.J., Wang, J.: Solving propositional satisfiability problems, Annals of Mathematics and Artificial Intelligence 1, 167 – 188 (1990). 
    12. Lecoutre, C., Sais, L., Vion, J.: Using SAT Encodings to Derive CSP Value Ordering Heuristics, Journal of Satisfiability, Boolean Modeling and Computation 1, 169-186, (2007).  
    13. Mackworth, A.: Consistency in networks of relations, Artificial Intelligence 8, 99–118 (1977). 
    14. Mali, A., Puthan Veettil, Sandhya: Adapting SAT Heuristics to Solve Constraint Satisfaction Problems, Technical Report, University of Wisconsin, (2001). 
    15. Marques – Silva, J.P., Sakallah, K.A.: GRASP - A new search algorithm for satisfiability, International Conference on Computer Aided Design (ICCAD), 220 – 227 (1996). 
    16. Moskewicz, M.W., Madigan, C.F., Zhao, Y., Zhang, L., Malik, S.: Chaff: Engineering an efficient SAT solver, International Design Automation Conference, 530 – 535 (2001). 
    17. Zabih, R., McAllester, D.A.: A rearrangement search strategy for determining propositional satisfiability, AAAI, 155-160 (1988).
    
    

